﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCMSHomepageFacebook.ascx.cs"
    Inherits="SydneyAviators.Web.Controls.ucCMSHomepageFacebook" %>
<%@ Register Src="~/Controls/ucFooter.ascx" TagName="ucFooter" TagPrefix="uc" %>
<div class="face">
    <div id="facebook-container">
        <div class="facebook-wrapper">
            <div class="facebook-tile">
                <h2>
                    Follow us on Facebook</h2>
            </div>
            <div id="facebook">
                <div id="likebox-frame">
                    <iframe src='<%= this.URL %>' scrolling="no" frameborder="0" style="border: none; overflow: hidden; width: 887px;
                        height: 395px;" allowtransparency="true"></iframe>
                </div>
            </div>
        </div>
       <uc:ucFooter ID="ctlFooter" runat="server" />
    </div>
</div>

