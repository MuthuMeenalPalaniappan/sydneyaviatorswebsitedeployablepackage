﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucHeader.ascx.cs" Inherits="SydneyAviators.Web.Controls.ucHeader" %>

    <a class="logo" href='<%= Page.ResolveUrl("~/Default.aspx") %>' title="Return to Sydney Aviators Homepage">
        Sydney Aviators</a>     
    <div class="nav-wrap">
        <div class="nav-wrapL">
            <ul class="nav navL">
                <li>
                    <asp:HyperLink ID="lnkLearnToFly" runat="server"><span>Learn to Fly</span></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkAircraftHire" runat="server"><span>Aircraft Hire</span></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkEvents" runat="server"><span>Events</span></asp:HyperLink></li>
            </ul>
        </div>
        <div class="nav-wrapR">
            <ul class="nav navR">
                <li>
                    <asp:HyperLink ID="lnkResources" runat="server"><span>Resources</span></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkAboutUs" runat="server"><span>About Us</span></asp:HyperLink></li>
                <li>
                    <asp:HyperLink ID="lnkContact" runat="server"><span>Contact</span></asp:HyperLink></li>
            </ul>
        </div>
    </div>
    <!--/nav-wrap-->
  