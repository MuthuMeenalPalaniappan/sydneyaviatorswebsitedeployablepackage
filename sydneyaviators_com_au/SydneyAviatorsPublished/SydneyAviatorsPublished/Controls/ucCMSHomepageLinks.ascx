﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCMSHomepageLinks.ascx.cs"
    Inherits="SydneyAviators.Web.Controls.ucCMSHomepageLinks" %>
<asp:Repeater ID="rptLinkCategories" runat="server" OnItemDataBound="rptLinkCategories_ItemDataBound">
    <ItemTemplate>
        <h4><asp:Literal ID="litLinkCategory" runat="server" /></h4>
        <asp:Repeater ID="rptLinks" runat="server" OnItemDataBound="rptLinks_ItemDataBound">
            <HeaderTemplate>
                <ul class="links">
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                <asp:HyperLink ID="lnkLink" runat="server" /></li>
            </ItemTemplate>
            <FooterTemplate>
                </ul></FooterTemplate>
        </asp:Repeater>
    </ItemTemplate>
</asp:Repeater>
