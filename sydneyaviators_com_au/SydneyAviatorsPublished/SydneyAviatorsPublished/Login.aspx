﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Main.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="SydneyAviators.Web.Login" %>
    <asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
    <asp:Content ID="banner" ContentPlaceHolderID="banner" runat="server">
    <div class="banner">
        <div class="banner-title">
            <h1>
                <asp:Literal ID="litPageTitle" runat="server" />&nbsp;
                <asp:Literal ID="litSEOTitle" runat="server" /></h1>
        </div>
        <asp:HyperLink ID="lnkEnquiry" runat="server" CssClass="btn-enquiry">Make an enquiry</asp:HyperLink>
    </div>
    <!--/banner-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">

    <div class="wrapper fullwidth">
         <div class="main">
            <div class="content">
                <div class="form">
                    <h4>
                        Your Login</h4>                        
            <table class="form-table" cellspacing="0">
            <tr>
                <td>
                    <asp:Literal ID="litError" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Username:
                </td>
                <td>
                    <asp:TextBox ID="txtUsername" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Password:
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" AutoCompleteType="None" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Button ID="btnLogin" class="btn right" OnClick="btnLogin_Click" runat="server" Text="Login" Visible = "false" />
                    <asp:LinkButton runat="server" ID="lnkLogin" class="btn right" Text="<span>Login</span>" OnClick="btnLogin_Click"></asp:LinkButton>                                   
                </td>
            </tr>
        </table>
        </div>
        </div>
        </div>
    </div>
   
    </asp:Content>
