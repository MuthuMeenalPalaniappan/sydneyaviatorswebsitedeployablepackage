﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Main.Master" AutoEventWireup="true"
    CodeBehind="Resource.aspx.cs" Inherits="SydneyAviators.Web.Resource" %>

<%@ Register Src="~/Controls/ucBreadcrumb.ascx" TagName="ucBreadcrumb" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucCMSMenu.ascx" TagName="ucCMSMenu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucCMSAttachment.ascx" TagName="ucCMSAttachment" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucFacebookComments.ascx" TagName="ucFacebookComments" TagPrefix="uc" %>

<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="banner" runat="server" ContentPlaceHolderID="banner">
    <div class="banner">
        <div class="banner-title">
            <h1>
                Resources<span>&nbsp;
                    <asp:Literal ID="litSEOTitle" runat="server" /></span></h1>
        </div>
        <asp:HyperLink ID="lnkEnquiry" runat="server" CssClass="btn-enquiry">Make an enquiry</asp:HyperLink>
    </div>
    <!--/banner-->
</asp:Content>
<asp:Content ID="body" runat="server" ContentPlaceHolderID="body">
    <asp:Panel ID="divWrapper" runat="server" CssClass="wrapper">
        <uc:ucBreadcrumb ID="ctlBreadcrumb" runat="server" />
        <div class="main">
            <div class="content">
                <h2>
                    <asp:Literal ID="litTitle" runat="server" /></h2>
                <p class="intro">
                    <asp:Literal ID="litSummary" runat="server" /></p>
                <asp:Image ID="imgThumbnail" runat="server" CssClass="thumb left" />
                <asp:Literal ID="litContent" runat="server" />
                 <uc:ucFacebookComments ID="ctlFacebookComments" runat="server" />
            </div>
            <!--/content-->
            <div class="sup-content">
                <uc:ucCMSAttachment ID="ctlCMSAttachment" runat="server" />
            </div>
            <!--/sup-content-->
        </div>
        <!--/main-->
        <uc:ucCMSMenu ID="ctlCMSMenu" runat="server" />
    </asp:Panel>
    <!--/wrapper-->
</asp:Content>
