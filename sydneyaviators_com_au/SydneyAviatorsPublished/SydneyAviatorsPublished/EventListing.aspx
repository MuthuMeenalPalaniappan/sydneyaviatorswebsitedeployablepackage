﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/Main.Master" AutoEventWireup="true"
    CodeBehind="EventListing.aspx.cs" Inherits="SydneyAviators.Web.EventListing" %>

<%@ Register Src="~/Controls/ucBreadcrumb.ascx" TagName="ucBreadcrumb" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucCMSMenu.ascx" TagName="ucCMSMenu" TagPrefix="uc" %>
<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="banner" runat="server" ContentPlaceHolderID="banner">
    <div class="banner">
        <div class="banner-title">
            <h1>
                Events<span>&nbsp;
                    <asp:Literal ID="litSEOTitle" runat="server" /></span></h1>
        </div>
        <asp:HyperLink ID="lnkEnquiry" runat="server" CssClass="btn-enquiry">Make an enquiry</asp:HyperLink>
    </div>
    <!--/banner-->
</asp:Content>
<asp:Content ID="body" runat="server" ContentPlaceHolderID="body">
    <div class="wrapper">
        <uc:ucBreadcrumb ID="ctlBreadcrumb" runat="server" />
        <div class="main">
            <div class="content">
                <h2>
                    <asp:Literal ID="litTitle" runat="server" /></h2>
                <div class="listing-grid">
                    <asp:Repeater ID="rptItems" runat="server" OnItemDataBound="rptItems_ItemDataBound">
                        <ItemTemplate>
                            <asp:PlaceHolder ID="phRowStart" runat="server" Visible="false">
                                <div class="row">
                            </asp:PlaceHolder>
                            <div class="item">
                                <div class="img">
                                    <asp:HyperLink ID="lnkThumbnail" runat="server">
                                        <asp:Image ID="imgThumbnail" runat="server" CssClass="thumb" /></asp:HyperLink></div>
                                <div class="copy">
                                    <span class="sml-title">
                                        <asp:Literal ID="litDate" runat="server" /></span>
                                    <h4>
                                        <asp:HyperLink ID="lnkTitle" runat="server" /></h4>
                                    <p>
                                        <asp:Literal ID="litSummary" runat="server" />...<asp:HyperLink ID="lnkMore" runat="server"
                                            CssClass="more" Text="more" /></p>
                                </div>
                            </div>
                            <!--/item-->
                            <asp:PlaceHolder ID="phRowEnd" runat="server" Visible="false"></div> </asp:PlaceHolder>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <!--/listing-grid-->
            </div>
            <iframe id="iframeExternalForm" runat="server" class="externalForm" frameborder="0"
                style="width: 100%;" visible="false"></iframe>
            <!--/content-->
        </div>
        <!--/main-->
        <uc:ucCMSMenu ID="ctlCMSMenu" runat="server" />
    </div>
    <!--/wrapper-->
</asp:Content>
