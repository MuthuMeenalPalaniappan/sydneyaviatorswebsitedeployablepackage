﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucCMSCourseListingDetails.ascx.cs"
    Inherits="SydneyAviators.Web.Admin.CMSCourse.Controls.ucCMSCourseListingDetails" %>
<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls"
    TagPrefix="cc" %>
<%@ Register Src="~/Admin/Controls/ucModuleHeader.ascx" TagName="ucModuleHeader"
    TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucButton.ascx" TagName="ucButton" TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucMessageBox.ascx" TagName="ucMessage" TagPrefix="uc" %>
<div class="module">
    <asp:UpdatePanel ID="upHeader" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc:ucModuleHeader ID="ctlModuleHeader" runat="server" TitleText="Course Listing Details"
                ControlToToggle="" LoadCollapsed="false" ShowEditButton="true" OnEdit="ctlModuleHeader_Edit"
                OnCancel="ctlModuleHeader_Cancel" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="clear">
        &nbsp;</div>
    <div class="pane">
        <p>
            Enter the following details about the course listing page.</p>
        <div class="form-wrap">
            <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table id="tblContent" runat="server" class="detail-table" border="0" cellpadding="0"
                        cellspacing="0" width="">
                        <tr>
                            <td width="20%">
                                <label>
                                    Title
                                    <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle"
                                        ErrorMessage="Please enter a title" SetFocusOnError="true" EnableClientScript="true"
                                        validationErrorCssClass="validationError" Display="Dynamic">*</asp:RequiredFieldValidator></label>
                            </td>
                            <td width="30%">
                                <asp:Literal ID="litTitle" runat="server" />
                                <asp:TextBox ID="txtTitle" runat="server" CssClass="text span-4" />
                            </td>
                            <td width="20%">
                                <label>
                                    Status</label>
                            </td>
                            <td width="30%">
                                <asp:Literal ID="litStatus" runat="server" />
                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="select span-4" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Summary</label>
                            </td>
                            <td colspan="3">
                                <asp:Literal ID="litSummary" runat="server" />
                                <cc:HTMLEditor ID="txtSummary" runat="server" Height="250" />
                            </td>
                        </tr>
                        <tr id="trExternalForm" runat="server" visible="false">
                            <td>
                                External Forms
                            </td>
                            <td colspan="3">
                                <asp:CheckBox ID="chkExternalForm" runat="server" Text="Add an external form" />
                                <div id="divExternalForm" runat="server" style="display: none;">
                                    <asp:TextBox ID="txtExternalForm" runat="server" TextMode="MultiLine" Columns="90"
                                        Rows="20" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Edit" />
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Cancel" />
                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upButtons" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divButtonBar" runat="server" class="actions" visible="false">
                        <div class="left">
                        </div>
                        <div class="right">
                            <uc:ucButton ID="btnSave" CssClass="btn" Text="Save" runat="server" OnClick="btnSave_Click"
                                CausesValidation="true" />
                        </div>
                    </div>
                    <uc:ucMessage ID="ctlMessage" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Edit" />
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Cancel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
<script language="javascript" type="text/javascript">
    function encodeExternalFormHtml(toEncode) {
        return toEncode.replace(/&/gi, '&amp;').replace(/\"/gi, '&quot;').replace(/</gi, '&lt;').replace(/>/gi, '&gt;');
    }
</script>
