﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucCMSHomepageLinkSearchFilter.ascx.cs" Inherits="SydneyAviators.Web.Admin.CMSHomepage.Controls.ucCMSHomepageLinkSearchFilter" %>


<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls" TagPrefix="cc" %>
<%@ Register Src="~/Admin/Controls/ucModuleHeader.ascx" TagName="ucModuleHeader" TagPrefix="uc" %>
<div class="module">
    <uc:ucModuleHeader ID="moduleHeaderFilter" ControlToToggle="pnlFilter" LoadCollapsed="true"
        TitleText="Search Promo Images" runat="server" />
    <asp:Panel ID="pnlFilter" runat="server" CssClass="pane">
        <p>
            Enter the following details to search for the required homepage link(s).</p>
        <div class="wrap">
            <table class="form-table" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="20%">
                        <label>
                            Keywords</label>
                    </td>
                    <td width="30%">
                        <asp:TextBox ID="txtKeywords" runat="server" CssClass="text span-4" />
                    </td>
                    <td width="20%">
                        <label>
                            Status
                        </label>
                    </td>
                    <td width="30%">
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="select span-4" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="actions">

            <div class="right">
                <cc:SubmitButton ID="btnSearch" Text="Search" OnClick="btnSearch_Click" runat="server" />
                <cc:BackButton ID="btnReset" Text="Reset" OnClick="btnReset_Click" runat="server" />
            </div>
        </div>
    </asp:Panel>
</div>
