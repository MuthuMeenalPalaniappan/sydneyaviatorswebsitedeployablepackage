<%@ Page Language="C#" AspCompat="TRUE" Debug="true" Inherits="OboutInc.oboutAJAXPage" %>

<%@ Import Namespace="obout_ASPTreeView_2_NET" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>
<%@ Register TagPrefix="ogrid" Namespace="Obout.Grid" Assembly="obout_Grid_NET" %>
<%@ Register TagPrefix="oajax" Namespace="OboutInc" Assembly="obout_AJAXPage" %>
<%@ Register TagPrefix="obspl" Namespace="OboutInc.Splitter2" Assembly="obout_Splitter2_Net" %>

<script language="C#" runat="server">
    string initialDirectory = "../../../uploads";
    string DefaultFolder;
    int expandedLevel = 0;

    void Page_Load(object sender, EventArgs e)
    {
        // try to upload the file
        try
        {
            if (fileDocument.PostedFile.FileName.Trim() != string.Empty)
            {
                //LibraryControl lib = new LibraryControl();
                //litUpload.Text = "<script language=\"javascript\" type=\"text/javascript\">";
                //litUpload.Text += "alert('" + lib.UploadDocument(fileDocument, hidDir.Value) + "');";
                //litUpload.Text += "</";
                //litUpload.Text += "script>";
            }
        }
        catch {}
            
        DefaultFolder = Server.MapPath(initialDirectory);

        ThrowExceptionsAtClient = false;
        ShowErrorsAtClient = false;

        LoadTreeView();

        // set default initial directory
        if (!IsPostBack)
            SelectDir(initialDirectory);

    }

    private void LoadTreeView()
    {
        obout_ASPTreeView_2_NET.Tree oTree = new obout_ASPTreeView_2_NET.Tree();
        oTree.id = "tree_0";
        string ParentID = "root";

        oTree.AddRootNode("<span style='cursor:pointer'>Document Library</span>", null);

        DirectoryInfo rootFolder = new DirectoryInfo(DefaultFolder);
        LoadDirRecursive(ParentID, rootFolder, oTree);

        oTree.Width = "150px";
        oTree.DragAndDropEnable = false;
        oTree.KeyNavigationEnable = true;
        oTree.SelectedEnable = true;
        oTree.SelectedId = "root_tree_0";
        oTree.EventList = "OnNodeSelect";

        oTree.FolderIcons = "../ASPTreeView/tree2/icons";
        oTree.FolderScript = "../ASPTreeView/tree2/script";
        oTree.FolderStyle = "../ASPTreeView/tree2/style/Explorer";

        TreeView.Text = oTree.HTML();
    }

    private void LoadDirRecursive(string ParentID, DirectoryInfo rootFolder, obout_ASPTreeView_2_NET.Tree oTree)
    {
        string nodeId;
        expandedLevel++;

        foreach (DirectoryInfo dir in rootFolder.GetDirectories())
        {
            string dirName = dir.Name;
            string dirID = dirName;

            bool expanded = true;
            if (expandedLevel >= 15)
                expanded = false;

            nodeId = ParentID == "root" ? "Library_documents_" : ParentID + "_";
            nodeId += dirID;

            string textDirName = "<span style='cursor:pointer'>" + dirName + "</span>";
            oTree.Add(ParentID, nodeId, textDirName, expanded, "folder.gif", null);

            LoadDirRecursive(nodeId, new DirectoryInfo(rootFolder + "/" + dirName), oTree);
        }
    }

    // populate grid with directory content
    public void SelectDir(string dirID)
    {
        ViewState["dirID"] = dirID;

        LoadGrid();

        UpdatePanel("cpDir");
    }

    public void LoadGrid()
    {
        string dirID = ViewState["dirID"].ToString();

        DataSet dsDir = new DataSet();
        dsDir.Tables.Add(new DataTable());
        dsDir.Tables[0].Columns.Add(new DataColumn("name", System.Type.GetType("System.String")));
        dsDir.Tables[0].Columns.Add(new DataColumn("size", System.Type.GetType("System.Int32")));
        dsDir.Tables[0].Columns.Add(new DataColumn("type", System.Type.GetType("System.String")));
        dsDir.Tables[0].Columns.Add(new DataColumn("datemodified", System.Type.GetType("System.String")));
        dsDir.Tables[0].Columns.Add(new DataColumn("imageType", System.Type.GetType("System.String")));

        if (dirID == "root_tree_0")
            dirID = initialDirectory;

        DirectoryInfo rootFolder = new DirectoryInfo(Server.MapPath(dirID.Replace("_", "/")));

        foreach (DirectoryInfo dir in rootFolder.GetDirectories())
        {
            string dirName = dir.Name;
            string dirDateTime = dir.LastAccessTime.ToString("d/M/yyyy h:m:s tt");
            string dirImageType = "Folder";
            dsDir.Tables[0].Rows.Add(new object[] { dirName, 0, "File Folder", dirDateTime, dirImageType });
        }

        foreach (FileInfo file in rootFolder.GetFiles())
        {
            string fileName = file.Name;
            string fileSize = file.Length.ToString();
            string fileType = file.Extension.Replace(".", "");
            string fileImageType = "File";
            string fileDateTime = file.LastAccessTime.ToString("d/M/yyyy h:m:s tt");

            dsDir.Tables[0].Rows.Add(new object[] { fileName, fileSize, fileType, fileDateTime, fileImageType });
        }

        gridDir.DataSource = dsDir;
        gridDir.DataBind();
    }
	
</script>

<script type="text/javascript">

function SelectDir(dirID)
{
	ob_post.AddParam("dirID", dirID);
	ob_post.post(null, "SelectDir", function(){});
}
function uploadDocument() {
    //var dirID = document.getElementById('hidDir').value;
    //var file = document.getElementById('fileDocument');
    //LibraryControl.UploadDocument(file, dirID, OnComplete, OnTimeOut, OnError);
    document.Form1.submit();
}
function newFolder() {
    document.getElementById('trNewFolder').style.display = "";
    document.getElementById('txtNewFolder').value = "";
}
function deleteFolder() {
    if (confirm('Are you sure you want to delete this folder and its contents?'))
    {
        var dirID = document.getElementById('hidDir').value;
        LibraryControl.DeleteFolder(document.getElementById('txtNewFolder').value, dirID, OnComplete, OnTimeOut, OnError);
    } 
}
function newFolderCancel() {
    document.getElementById('trNewFolder').style.display = "none";
}
function createFolder() {
    var dirID = document.getElementById('hidDir').value;
    LibraryControl.CreateFolder(document.getElementById('txtNewFolder').value, dirID, OnComplete, OnTimeOut, OnError);
}
function OnComplete(arg) {
    alert(arg);
    if (arg.indexOf('Error') == -1) {
        window.location.reload();
    } 
}
function OnTimeOut(arg) {
    alert("TimeOut encountered when calling Say Hello.");
}
function OnError(arg) {
    alert("Error encountered when calling Say Hello.");
}
function showFile() {
    var filetype = gridDir.SelectedRecords[0].Type;
    //alert(gridDir.SelectedRecords[0].Type);
    if (filetype != "File Folder") {
        var dirID = document.getElementById('hidDir').value;
        //alert(dirID);
        window.open(dirID.replace(/_/g, "/") + "/" + gridDir.SelectedRecords[0].Name);
    }
}
</script>

<html>
<head>
    <title>Document Library</title>
    <link href="../../AdminStyles.css" rel="stylesheet" type="text/css" />
</head>
<body style="padding: 10px;">
    <form id="Form1" runat="server">
        <h1>
            Document Library</h1>
        <br />
        <input type="hidden" id="hidDir" value="root_tree_0" runat="server" />
        <asp:Literal ID="litUpload" runat="server"></asp:Literal>
        Upload file<br />
        <asp:FileUpload ID="fileDocument" runat="server" CssClass="btn" style="color:#585956;" /><input type="button" value="Upload" class="btn" onclick="uploadDocument();" /><br />
        <input type="button" value="New Folder" class="btn" onclick="newFolder();" /><input type="button" value="Delete Folder" class="btn" class="btn" onclick="deleteFolder();" />
        <table border="0">
            <tr id="trNewFolder" style="display: none;">
                <td>
                    <input id="txtNewFolder" type="text" class="formField" /><input type="button" value="Create Folder" class="btn"
                        onclick="createFolder();" /><input type="button" value="Cancel" class="btn" onclick="newFolderCancel();" />
                </td>
            </tr>
            <tr>
                <td>NOTE: double click on file to open</td>
            </tr>
            <tr>
                <td valign="top" class="h5">
                    <div style="border: 1px solid gray; width: 680px; height: 370px;">
                        <div style="width: 680px; height: 370px;">
                            <obspl:Splitter ID="sp1" runat="server" StyleFolder="Splitter/styles/default">
                                <LeftPanel WidthDefault="169" WidthMin="169" WidthMax="350">
                                    <Header Height="30">
                                        <div style="padding-left: 10px; padding-top: 5px; padding-bottom: 5px; background-color: #C0C0C0"
                                            class="tdText">
                                            <b style="font-size: 10px">Folders</b>
                                        </div>
                                    </Header>
                                    <Content>
                                        <div style="padding-top: 7px; padding-left: 10px; border-top: 1px solid gray">
                                            <asp:Label ID="TreeView" runat="server">Label</asp:Label>
                                        </div>
                                    </Content>
                                </LeftPanel>
                                <RightPanel>
                                    <Content>
                                        <div style="padding-top: 0px; padding-left: 0px;">
                                            <oajax:CallbackPanel ID="cpDir" runat="server">
                                                <Content>
                                                    <ogrid:Grid ID="gridDir" runat="server" AllowRecordSelection="true" ShowFooter="false"
                                                        KeepSelectedRecords="false" AllowAddingRecords="false" CallbackMode="true" Serialize="false"
                                                        AllowColumnResizing="true" ShowHeader="true" Width="100%" PageSize="100" FolderStyle="../grid/styles/style_5"
                                                        AutoGenerateColumns="false">
                                                        <ClientSideEvents OnClientDblClick="showFile" />
			                                            <Columns>
                                                            <ogrid:Column ID="Column1" DataField="imageType" HeaderText="" Width="53" runat="server"
                                                                TemplateId="tplImageType" />
                                                            <ogrid:Column ID="Column2" DataField="Name" HeaderText="Name" Width="120" runat="server" />
                                                            <ogrid:Column ID="Column3" DataField="Size" HeaderText="Size" Width="80" runat="server"
                                                                TemplateId="tplSize" />
                                                            <ogrid:Column ID="Column4" DataField="Type" HeaderText="Type" Width="83" runat="server" />
                                                            <ogrid:Column ID="Column5" DataField="DateModified" HeaderText="Date Modified" Width="167"
                                                                runat="server" />
                                                        </Columns>
                                                        <Templates>
                                                            <ogrid:GridTemplate runat="server" ID="tplImageType">
                                                                <Template>
                                                                    <div style="width: 100%; height: 100%; text-align: right; padding-top: 4px">
                                                                        <img src="Splitter/images/filebrowser/<%# Container.Value %>.gif" />
                                                                    </div>
                                                                </Template>
                                                            </ogrid:GridTemplate>
                                                            <ogrid:GridTemplate runat="server" ID="tplSize">
                                                                <Template>
                                                                    <div style="width: 100%; height: 100%; padding-left: 10px; padding-top: 6px">
                                                                        <%# Container.Value == "0" ? "" : Container.Value + " KB" %>
                                                                    </div>
                                                                </Template>
                                                            </ogrid:GridTemplate>
                                                        </Templates>
                                                    </ogrid:Grid>
                                                </Content>
                                                <Loading>
                                                    <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <img src="Splitter/images/loading_icons/1.gif">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </Loading>
                                            </oajax:CallbackPanel>
                                            <oajax:CallbackPanel ID="cpLabel" runat="server">
                                                <Content>
                                                    <asp:Label ID="lblDir" runat="server" />
                                                </Content>
                                            </oajax:CallbackPanel>
                                        </div>
                                    </Content>
                                </RightPanel>
                            </obspl:Splitter>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <br />
    </form>
</body>
</html>

<script type="text/javascript">
function ob_OnNodeSelect(id)
{   
    document.getElementById('hidDir').value = id;
	document.getElementById('trNewFolder').style.display = "none";
	SelectDir(id);
}

</script>

