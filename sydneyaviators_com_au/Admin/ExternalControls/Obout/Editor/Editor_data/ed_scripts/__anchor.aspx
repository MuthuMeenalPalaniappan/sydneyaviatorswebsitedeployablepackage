<%@ Page Language="C#" %>
<%@ Register TagPrefix="ed" Namespace="OboutInc.Editor" Assembly="obout_Editor" %>
<script runat="server">

OboutInc.Editor.FieldsFiller anchor_filler;

void Page_Load(object o, EventArgs e)
{
  anchor_filler = new OboutInc.Editor.FieldsFiller(Page,"anchor",Page.Request["localization_path"],Page.Request["language"]);
}
</script>
<html>
<head runat="server">
</head>
<body>
<FIELDSET style="margin: 0px; margin-top: 2px; margin-left: 2px; padding: 4px; text-align: center;">
<%= anchor_filler.Get("name","Anchor name") %>:
<input id="anchor_name" type="text" maxlength="20" size="20" value="">
</FIELDSET>
<div style='margin-top:2px;'>
<span class='box' style='float:right; margin-right:5px;'>
<div id='cancel' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= anchor_filler.Get("cancel","Cancel") %></div>
</span>
<span class='box' style='float:right; margin-right:5px;'>
<div id='ok' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= anchor_filler.Get("ok","OK") %></div>
</span>
</div>
</body>
</html>
