<%@ Page Language="C#" %>
<%@ Register TagPrefix="ed" Namespace="OboutInc.Editor" Assembly="obout_Editor" %>
<script runat="server">

OboutInc.Editor.FieldsFiller filler;

void Page_Load(object o, EventArgs e)
{
  filler = new OboutInc.Editor.FieldsFiller(Page,"link-browse",Page.Request["localization_path"],Page.Request["language"]);
}
</script>
<html>
<head runat="server">
</head>
<body>
<table border="0" style="margin-right:4px;">
<tr>
<td align="right">
<iframe id='innerIframe' style="width:400px;height:400px;overflow:auto;margin:0px;margin-bottom:4px;margin-top:4px;margin-left:4px;">
</iframe>
</td>
</tr>
<tr>
<td align="right">
<div style='margin-top:2px;'>
<span class='box' style='float:right; margin-right:5px;'>
<div id='cancel' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= filler.Get("cancel","Cancel") %></div>
</span>
<span class='box' style='float:right; margin-right:5px;'>
<div id='ok' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= filler.Get("ok","OK") %></div>
</span>
</div>
</td>
</tr>
</table>
</body>
</html>