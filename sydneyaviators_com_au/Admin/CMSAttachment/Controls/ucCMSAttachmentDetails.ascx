﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCMSAttachmentDetails.ascx.cs"
    Inherits="SydneyAviators.Web.Admin.CMSAttachment.Controls.ucCMSAttachmentDetails" %>
<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls"
    TagPrefix="cc" %>
<%@ Register Src="~/Admin/Controls/ucModuleHeader.ascx" TagName="ucModuleHeader"
    TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucButton.ascx" TagName="ucButton" TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucMessageBox.ascx" TagName="ucMessage" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucAJAXFileUpload.ascx" TagName="ucAJAXFileUpload" TagPrefix="uc" %>
<div class="module">
    <asp:UpdatePanel ID="upHeader" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc:ucModuleHeader ID="ctlModuleHeader" runat="server" TitleText="Download Details"
                ControlToToggle="" LoadCollapsed="false" ShowEditButton="true" OnEdit="ctlModuleHeader_Edit"
                OnCancel="ctlModuleHeader_Cancel" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="clear">
        &nbsp;</div>
    <div class="pane">
        <p>
            Enter the following details about the download.</p>
        <div class="form-wrap">
            <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table id="tblContent" runat="server" class="detail-table" border="0" cellpadding="0"
                        cellspacing="0" width="">
                        <tr>
                            <td width="20%">
                                <label>
                                    Title
                                    <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle"
                                        ErrorMessage="Please enter a title" SetFocusOnError="true" EnableClientScript="true"
                                        validationErrorCssClass="validationError" Display="Dynamic">*</asp:RequiredFieldValidator></label>
                            </td>
                            <td width="30%">
                                <asp:Literal ID="litTitle" runat="server" />
                                <asp:TextBox ID="txtTitle" runat="server" CssClass="text span-4" />
                            </td>
                            <td width="20%">
                                <label>
                                    Status</label>
                            </td>
                            <td width="30%">
                                <asp:Literal ID="litStatus" runat="server" />
                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="select span-4" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Attachment Type</label>
                            </td>
                            <td>
                                <asp:Literal ID="litAttachmentType" runat="server" />
                                <asp:DropDownList ID="ddlAttachmentType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAttachmentType_SelectedIndexChanged" CausesValidation="false" CssClass="select span-4" />
                            </td>
                            <td>
                                <label>
                                </label>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="trAttachmentFile" runat="server">
                            <td>
                                <label>
                                    Attachment</label>
                            </td>
                            <td colspan="3">
                                <uc:ucAJAXFileUpload ID="filAttachment" runat="server" />
                                <br />
                                <asp:HyperLink ID="lnkAttachment" runat="server" Text="View" Target="_blank" />
                                <asp:LinkButton ID="btnDeleteAttachment" runat="server" Text="Delete" OnClick="btnDeleteAttachment_Click"
                                    OnClientClick="return confirm('Are you sure you want to delete this?');" />
                            </td>
                        </tr>

                         <tr id="trAttachmentWebLink" runat="server" visible="false">
                            <td>
                                <label>
                                    Url</label>
                            </td>
                            <td colspan="3">
                                <asp:HyperLink ID="lnkUrl" runat="server" Target="_blank" />
                                <asp:TextBox ID="txtUrl" runat="server" CssClass="text span-4" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label>
                                    Description</label>
                            </td>
                            <td colspan="3">
                                <asp:Literal ID="litDescription" runat="server" />
                                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Rows="3" CssClass="text span-4" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Edit" />
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Cancel" />
                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="ddlAttachmentType" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upButtons" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divButtonBar" runat="server" class="actions" visible="false">
                        <div class="left">
                        </div>
                        <div class="right">
                            <uc:ucButton ID="btnSave" CssClass="btn" Text="Save" runat="server" OnClick="btnSave_Click"
                                CausesValidation="true" />
                        </div>
                    </div>
                    <uc:ucMessage ID="ctlMessage" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnDeleteAttachment" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Edit" />
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Cancel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
