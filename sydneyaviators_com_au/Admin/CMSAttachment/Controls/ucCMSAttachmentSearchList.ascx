﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCMSAttachmentSearchList.ascx.cs" Inherits="SydneyAviators.Web.Admin.CMSAttachment.Controls.ucCMSAttachmentSearchList" %>
<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls"
    TagPrefix="cc" %>
<%@ Register Src="~/Admin/Controls/ucModuleHeader.ascx" TagName="ucModuleHeader" TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucPager.ascx" TagName="ucPager" TagPrefix="uc" %>
<div class="module">
    <uc:ucModuleHeader ID="moduleHeaderResults" ControlToToggle="pnlSearchResults" TitleText="Downloads"
        runat="server" />
    <div class="clear">
        &nbsp;</div>
    <asp:Panel ID="pnlSearchResults" CssClass="pane" runat="server">
        <asp:Panel ID="pnlAdd" runat="server" CssClass="actions">
            <div class="left">
            <asp:LinkButton ID="btnAdd" CssClass="btn" runat="server" OnClick="btnAdd_Click"><span>Add</span></asp:LinkButton></div>
            <div class="right">
            </div>
            <div class="clear">
                &nbsp;</div>
        </asp:Panel>
        <div class="listing-wrap">
            <asp:UpdatePanel ID="upGrid" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <cc:CustomGridView2 ID="grdItems" runat="server" CssClass="listing-table" OnRowDataBound="grdItems_RowDataBound"
                        CellSpacing="0" OnDataBound="grdItems_DataBound">
                        <AlternatingRowStyle CssClass="alt-row" />
                        <Columns>
                            <asp:TemplateField HeaderText="Up" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <center>
                                        <asp:ImageButton ID="btUp" SkinID="ibtUp" runat="server" OnClick="btGridCommand_Click" /></center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Down" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <center>
                                        <asp:ImageButton ID="btDown" SkinID="ibtDown" runat="server" OnClick="btGridCommand_Click" /></center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Title" HeaderStyle-Width="30%">
                                <ItemTemplate>
                                <asp:LinkButton ID="btnItem" runat="server" Text='<%# Eval("Title")%>' OnClick="btnItem_Click"  />
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="File" HeaderStyle-Width="30%">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkItem" runat="server" Text='View' NavigateUrl='<%# ResolveUrl(Eval("Url").ToString()) %>' Target="_blank" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <%# ((SydneyAviators.Business.Enums.Status)(Enum.Parse(typeof(SydneyAviators.Business.Enums.Status), Eval("StatusID").ToString()))).ToString()%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <center>
                                        <asp:LinkButton ID="btnDelete" CssClass="btn" runat="server" OnClick="btnDelete_Click"
                                            Width="40" OnClientClick="return confirm('Are you sure you want to delete this?');"><span>Delete</span></asp:LinkButton></center>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </cc:CustomGridView2>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ctlPager" EventName="PagerClick" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upPager" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <uc:ucPager ID="ctlPager" runat="server" OnPagerClick="ctlPager_PagerClick" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
</div>
