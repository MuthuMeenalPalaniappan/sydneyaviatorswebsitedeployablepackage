﻿<%@ Page Language="C#" MasterPageFile="~/Admin/MasterPage/Admin.Master" AutoEventWireup="True" CodeBehind="SettingSearch.aspx.cs" Inherits="SydneyAviators.Web.Admin.Setting.SettingSearch" %>

<%@ Register Src="~/Admin/Controls/ucCMSMenu.ascx" TagName="ucCMSMenu" TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucTitleBar.ascx" TagName="ucTitleBar" TagPrefix="uc" %>
<%@ Register Src="Controls/ucSettingSearchFilter.ascx" TagName="ucFilter" TagPrefix="uc" %>
<%@ Register Src="Controls/ucSettingSearchList.ascx" TagName="ucList" TagPrefix="uc" %>
<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="sidebar" runat="server" ContentPlaceHolderID="sidebar">
 <uc:ucCMSMenu ID="ctlCMSMenu" runat="server" />
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="main">

<asp:ScriptManager ID="ScriptManagerMain" runat="server" />
    <uc:ucTitleBar ID="ctlTitleBar" runat="server" TitleText="Settings" />
    <uc:ucFilter ID="ctlFilter" runat="server" OnSearch="ctlFilter_Search" OnReset="ctlFilter_Reset" SearchTopic="True" />
    <uc:ucList ID="ctlList" runat="server" />
</asp:Content>