﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucPager.ascx.cs" Inherits="SydneyAviators.Web.Admin.Controls.ucPager" %>

<div id="divPager" runat="server" class="actions" visible="false">
    <div class="left">
        <div class="pagination">
            <span id="spnPrevious" runat="server" class="previous-off">&laquo; Previous</span>
            <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPrevious_Click" OnClientClick="window.scrollTo(0,0);">&laquo; Previous</asp:LinkButton>
            <asp:Repeater ID="repPager" runat="server" OnItemDataBound="repPager_ItemDataBound">
                <ItemTemplate>
                    <span id="spnPager" runat="server" class="active">
                        <asp:Literal ID="litPager" runat="server" /></span>
                    <asp:LinkButton ID="btnPager" runat="server" OnClick="btnPager_Click" OnClientClick="window.scrollTo(0,0);"></asp:LinkButton>
                </ItemTemplate>
            </asp:Repeater>
            <span id="spnNext" runat="server" class="previous-off">Next &raquo;</span>
            <asp:LinkButton ID="btnNext" runat="server" OnClick="btnNext_Click" OnClientClick="window.scrollTo(0,0);">Next &raquo;</asp:LinkButton>
        </div>
        <!-- /.pagination -->
    </div>
</div>
<!-- /.actions -->
