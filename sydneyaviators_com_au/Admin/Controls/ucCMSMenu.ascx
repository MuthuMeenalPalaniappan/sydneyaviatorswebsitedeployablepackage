﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCMSMenu.ascx.cs" Inherits="SydneyAviators.Web.Admin.Controls.ucCMSMenu" %>

<div class="sub-nav">
    <p class="title"><asp:Literal ID="litTitle" runat="server" Text="Sydney Aviators Website" /></p>
    <asp:Repeater ID="rptMenu" runat="server" OnItemDataBound="rptMenu_ItemDataBound">
        <HeaderTemplate>
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <asp:HyperLink ID="lnkMenu" runat="server">
                    <span>
                        <asp:Literal ID="litMenu" runat="server" /></span></asp:HyperLink></li></ItemTemplate>
        <FooterTemplate>
            </ul></FooterTemplate>
    </asp:Repeater>
    <div class="clear">
        &nbsp;</div>
</div>
<!-- /.sub-nav -->
