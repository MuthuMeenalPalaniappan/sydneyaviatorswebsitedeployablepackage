﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucHeader.ascx.cs" Inherits="SydneyAviators.Web.Admin.Controls.ucHeader" %>
<%@ Register Src="ucBreadcrumb.ascx" TagName="ucBreadcrumb" TagPrefix="uc" %>
<%@ Register Src="ucCMSBreadcrumb.ascx" TagName="ucCMSBreadcrumb" TagPrefix="uc" %>
<div id="header">
    <div class="container">
        <div id="logo">
            <asp:HyperLink ID="lnkLogo" runat="server" NavigateUrl="~/Admin/Default.aspx">
                <asp:Image ID="imgLogo" runat="server" SkinID="imgLogo" /></asp:HyperLink></div>
        <div id="header-content">
            <div id="global-nav">
                <ul>
                    <li>
                        <asp:HyperLink ID="lnkHome" runat="server"><span>Home</span></asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="lnkSettings" runat="server"><span>Settings</span></asp:HyperLink></li>
                </ul>
                <div class="clear">
                    &nbsp;</div>
            </div>
            <!-- /#global-nav -->
            <uc:ucBreadcrumb ID="ctlBreadcrumb" runat="server" ShowOnPageTitle="true" />
            <uc:ucCMSBreadcrumb ID="ctlCMSBreadcrumb" runat="server" />
            <div id="user-info">
                <p>
                    <span class="name"><strong>Hello:</strong>
                        <asp:Literal ID="litContactName" runat="server" /></span> <span class="id"><strong>Account:</strong>
                            <asp:Literal ID="litUserName" runat="server" /></span>
                </p>
                <p>
                    <asp:LinkButton ID="btnLogout" CssClass="btn" OnClick="btnLogout_Click" runat="server"><span>Log out</span></asp:LinkButton></p>
                <div class="clear">
                    &nbsp;</div>
            </div>
            <!-- /#user-info -->
            <div class="clear">
                &nbsp;</div>
        </div>
        <!-- /#header-content -->
    </div>
    <!-- /.container -->
</div>
<!-- /#header -->
