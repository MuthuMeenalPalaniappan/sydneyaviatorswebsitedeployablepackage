﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucSearch.ascx.cs" Inherits="SydneyAviators.Web.Admin.Controls.ucSearch" %>
<div class="searchbar">
    <div class="search-field">
        <asp:TextBox ID="txtSearch" runat="server" /></div>
    <div class="search-btn">
        <asp:ImageButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CausesValidation="false" SkinID="btnSearch" /></div>
</div>
