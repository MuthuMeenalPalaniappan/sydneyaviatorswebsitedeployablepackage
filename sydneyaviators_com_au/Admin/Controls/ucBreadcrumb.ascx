﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucBreadcrumb.ascx.cs"
    Inherits="SydneyAviators.Web.Admin.Controls.ucBreadcrumb" %>
<p id="breadcrumbs">
    <asp:SiteMapPath ID="breadcrumb" runat="server" OnItemDataBound="breadcrumb_ItemDataBound"
        OnPreRender="breadcrumb_PreRender">
    </asp:SiteMapPath>
</p>
<!-- /#breadcrumbs -->
