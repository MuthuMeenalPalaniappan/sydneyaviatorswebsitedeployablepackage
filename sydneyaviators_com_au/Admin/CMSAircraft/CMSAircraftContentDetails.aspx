﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CMSAircraftContentDetails.aspx.cs"
    Inherits="SydneyAviators.Web.Admin.CMSAircraft.CMSAircraftContentDetails" MasterPageFile="~/Admin/MasterPage/Admin.Master" %>

<%@ Register Src="Controls/ucCMSAircraftContentInfo.ascx" TagName="ucInfo" TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucTitleBar.ascx" TagName="ucTitleBar" TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucCMSMenu.ascx" TagName="ucCMSMenu" TagPrefix="uc" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="sidebar" ContentPlaceHolderID="sidebar" runat="server">
    <uc:ucCMSMenu ID="ctlCMSMenu" runat="server" />
</asp:Content>
<asp:Content ID="main" ContentPlaceHolderID="main" runat="server">
    <asp:ScriptManager ID="ScriptManagerMain" runat="server" />
    <uc:ucTitleBar id="ctlTitle" runat="server" TitleText="Aircraft Content Details" />
    <uc:ucInfo ID="ctlInfo" runat="server" />
</asp:Content>
