// Multiple Content Tabs
$(document).ready(function() {

$.fn.module = function(){ 
	//Default Action
	$(this).find(".content").hide(); //Hide all content
	$(this).find("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(this).find(".content:first").show(); //Show first tab content
	
	//On Click Event
	$("ul.tabs li").click(function() {
		$(this).parent().parent().parent().find("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(this).parent().parent().parent().find(".content").hide(); //Hide all tab content
		var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
		$(activeTab).show(); //Fade in the active content
		return false;
	});
};//end function

$("div[class^='module']").module(); //Run function on any div with class name of "Simple Tabs"


});