﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucLogin.ascx.cs" Inherits="SydneyAviators.Web.Controls.ucLogin" %>
 <!-- Login -->
            <div class="login-nav">
            	<span class="login-text"><asp:Literal runat="server" ID="litLoginDetails" Text="Have an account?"></asp:Literal></span>              
                <a href="#" class="btn-login" runat="server" id="btnLogin" CausesValidation="false"><span>Login</span></a>
                <asp:LinkButton runat="server" ID="btnLogout" class="btn-logout" Text="<span>Logout</span>" OnClick="btnLogout_Click" Visible = "false" CausesValidation="false"></asp:LinkButton>
            </div>
            <div class="login-menu">
            	<div class="login-menu-inner">
                	<p>
                    	<label for="username">Username</label>
                         <asp:TextBox ID="txtUsername" runat="server" tabindex="4" class="text" CausesValidation="false"/>                    	
                    </p>

                    <p>
                        <label for="password">Password</label>
                         <asp:TextBox ID="txtPassword" runat="server" tabindex="5" class="text" AutoCompleteType="None" TextMode="Password"  CausesValidation="false"/>                       
                    </p>
                    <p>
                      <asp:LinkButton runat="server" ID="lnkLogin" class="btn" Text="<span>Login</span>" OnClick="btnLogin_Click" CausesValidation="false"></asp:LinkButton>
                    </p>
                    <p class="forgot">

                        <a href="#" id="#">Forgot your password?</a><br />
                        <a href="#">Forgot your username?</a>
                    </p>
                    <p style="color:Red">
                        <asp:Literal runat="server" ID="litError" Text=""></asp:Literal>
                    </p>
            	</div>
            </div>
			<script type="text/javascript">
			    $(document).ready(function () {

			        $(".btn-login").click(function (e) {
			            e.preventDefault();
			            $("div.login-menu").toggle();
			            $(".btn-login").toggleClass("login-menu-open");
			        });

			        $("div.login-menu").mouseup(function () {
			            return false
			        });
			        $(document).mouseup(function (e) {
			            if ($(e.target).parent("a.btn-login").length == 0) {
			                $(".btn-login").removeClass("login-menu-open");
			                $("div.login-menu").hide();
			            }
			        });

			    });

                function toggleLogin()
                {                   
                    $("div.login-menu").toggle();
                    $(".btn-login").toggleClass("login-menu-open");
                }
            </script>
            <!--/login-->
