﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucFooter.ascx.cs" Inherits="SydneyAviators.Web.Controls.ucFooter" %>
<style>
    #footer ul li a,h5{
       color: white; font-weight: bold;
    }
    #footer ul li a:hover{
        color:darkblue;
    }
    #divWithbg{
        padding: 5px; 
        background-image: linear-gradient(to top, #33AFFF 0%, #33C4FF 99%, #e3eeff 100%);
        background-image: -webkit-linear-gradient(top, #33AFFF, #33C4FF);
    }
</style>
<!--Footer-->
<br />
<div id="footer" style="margin-top:15px">

    <!--Footer Links-->
    <div id="divWithbg">
        <h5 style="text-align: center;">BASAIR AUSTRALIA NETWORK</h5>
        <br />

        <ul style="text-align: center; list-style-type: none;">
            <li><a  href="http://www.basair.com.au">Basair Aviation College</a></li>
            <li><a  href="https://www.australiabyair.com.au/">Australia By Air</a></li>
            <li><a  href="http://www.sydneyaviators.com.au">Sydney Aviators</a></li>
            <li><a  href="https://www.brisbaneaviators.com.au/">Brisbane Aviators</a></li>
            <li><a  href="https://www.huntervalleyaviation.com.au/">Hunter Valley Aviation</a></li>
            <li><a  href="https://uavair.com.au/">UAVAIR</a></li>
            <li><a  href="http://www.multicrewtraining.com/">MATS</a></li>
            <li><a  >Eagle Aircraft Maintenance </a></li>
        </ul>
    </div>
    <!--/.Footer Links-->

    <!--Copyright-->
    <div style="margin-top:10px">
        <p class="left">
            &copy; 2012
                            <asp:Literal ID="litCompanyName" runat="server" />.
                            <asp:HyperLink ID="lnkTerms" runat="server">Privacy Policy</asp:HyperLink>
            <br />
            Website by <a href="http://www.levo.com.au" title="www.levo.com.au">Levo</a> &copy;                       2010
        </p>
        <p class="right">
            Follow Us
        <asp:HyperLink ID="lnkFacebook" runat="server" ToolTip="Facebook" Target="_blank">
            <asp:Image ID="imgFacebook" runat="server" SkinID="imgFacebook" />
        </asp:HyperLink>&nbsp;<asp:HyperLink
            ID="lnkTwitter" runat="server" ToolTip="Twitter" Target="_blank">
            <asp:Image ID="imgTwitter" runat="server" SkinID="imgTwitter" />
        </asp:HyperLink>
        </p>
    </div>
    <!--/.Copyright-->

</div>
<!--/.Footer-->


