﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Main.Master" AutoEventWireup="true" CodeBehind="BookingConfirmation.aspx.cs" Inherits="SydneyAviators.Web.BookingConfirmation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="banner" runat="server">
 <div class="banner">
        <div class="banner-title">
            <h1>
                <asp:Literal ID="litPageTitle" runat="server" />&nbsp;
                <span><asp:Literal ID="litSEOTitle" runat="server" /></span></h1>
        </div>
        <asp:HyperLink ID="lnkEnquiry" runat="server" CssClass="btn-enquiry">Make an enquiry</asp:HyperLink>
    </div>
    <!--/banner-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
<div class="wrapper fullwidth">

            
        <p class="breadcrumbs">
            <a href="./">Home</a>
             <span class="sep">&gt; </span>
             <asp:HyperLink ID="lnkAircraftHire" runat="server"><span>Aircraft Hire</span></asp:HyperLink>
             <span class="sep">&gt; </span>Aircraft Booking
        </p>
            
            <div class="main">
                <div class="content">
                    
                    
                    <div class="confirmation">
                    	
                        <h2>Thank you your booking has been processed.</h2>

                        
                         <p>An email has been sent to you with a copy of the details below.</p>

                        
                      

                         <div class="form">
                            <h4>Your Booking Info</h4>
                            <div id="ctl00_body_valSummary" style="color:Red;display:none;"> </div>

                            <table class="form-table form-table-2cols" cellspacing="0">
                        <colgroup>
                                <col width="19%" />
                                <col width="31%" />
                                <col width="19%" />
                                <col width="31%" />
                            </colgroup>
                             <tr>
                                <td><label>Aircraft</label></td>
                                <td class="txt-col"> <asp:Literal ID="litResourceTitle" runat="server" /></td>
                                <td><label>Booking Date</label></td>
                                <td class="txt-col"><asp:Literal ID="litScheduleStartDate" runat="server" /></td>
                            </tr>
                            <tr>
                                <td><label>Type</label></td>
                                <td class="txt-col"><asp:Literal ID="litBookingType" runat="server" /></td>
                                <td><label>Start Time</label></td>
                                <td class="txt-col"><asp:Literal ID="litScheduleStartTime" runat="server" /></td>
                            </tr>
                             <tr>
                                <td><label>Rate</label></td>

                                <td class="txt-col"><asp:Literal ID="litRate" runat="server" /> / h</td>
                                <td><label>Booking Duration</label></td>
                                <td class="txt-col"><asp:Literal ID="litScheduleDuration" runat="server" /></td>
                            </tr>
                             <tr>
                                <td><label>Total</label></td>
                                <td class="txt-col"><asp:Literal ID="litTotal1" runat="server" /></td>

                                <td>&nbsp;</td>
                                <td class="txt-col">&nbsp;</td>
                            </tr>                    
                    </table>
                        </div>
                        
                    <a href="Default.aspx" class="btn"><span>Return to Home</span></a>

                        
                    </div>
                    
                    
                    
                </div> <!--/content-->
            </div> <!--/main-->

        </div> <!--/wrapper-->

</asp:Content>
