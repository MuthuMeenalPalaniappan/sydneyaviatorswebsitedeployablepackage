﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="ExternalForm.aspx.cs"
    Inherits="SydneyAviators.Web.ExternalForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Sydney Aviators</title>
</head>
<body>
    <asp:Literal ID="litExternalForm" runat="server" />
</body>
</html>
