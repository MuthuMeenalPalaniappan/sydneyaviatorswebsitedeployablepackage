﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Main.Master" AutoEventWireup="true"
    CodeBehind="Content.aspx.cs" Inherits="SydneyAviators.Web.Content" %>

<%@ Register Src="~/Controls/ucBreadcrumb.ascx" TagName="ucBreadcrumb" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucCMSMenu.ascx" TagName="ucCMSMenu" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="banner" runat="server">
    <div class="banner">
        <div class="banner-title">
            <h1>
                <asp:Literal ID="litPageName" runat="server" /><span>&nbsp;
                    <asp:Literal ID="litSEOTitle" runat="server" /></span></h1>
        </div>
        <asp:HyperLink ID="lnkEnquiry" runat="server" CssClass="btn-enquiry">Make an enquiry</asp:HyperLink>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="wrapper">
        <uc:ucBreadcrumb ID="ctlBreadcrumb" runat="server" />
        <div class="main">
            <div class="content">
                <h2>
                    <asp:Literal ID="litTitle" runat="server" /></h2>
                <p class="intro">
                    <asp:Literal ID="litSummary" runat="server" /></p>
                <asp:Literal ID="litContent" runat="server" />
            </div>
            <iframe id="iframeExternalForm" runat="server" class="externalForm" frameborder="0"
                style="width: 100%;" visible="false"></iframe>
            <!--/content-->
        </div>
        <!--/main-->
        <uc:ucCMSMenu ID="ctlCMSMenu" runat="server" />
    </div>
    <!--/wrapper-->
</asp:Content>
