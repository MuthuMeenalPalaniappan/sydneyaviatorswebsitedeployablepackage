﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/Main.Master" AutoEventWireup="true"
    CodeBehind="ResourceListing.aspx.cs" Inherits="SydneyAviators.Web.ResourceListing" %>

<%@ Register Src="~/Controls/ucBreadcrumb.ascx" TagName="ucBreadcrumb" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucCMSMenu.ascx" TagName="ucCMSMenu" TagPrefix="uc" %>
<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript" src="/js/vjustify.js"></script>
    <script type="text/javascript">
        // Equal Height Containers
        $(document).ready(function () {
            $(".equal-height .copy").vjustify();
        });		
    </script>
</asp:Content>
<asp:Content ID="banner" runat="server" ContentPlaceHolderID="banner">
    <div class="banner">
        <div class="banner-title">
      <h1>Resources<span>&nbsp;
                    <asp:Literal ID="litSEOTitle" runat="server" /></span></h1>
        </div>
        <asp:HyperLink ID="lnkEnquiry" runat="server" CssClass="btn-enquiry">Make an enquiry</asp:HyperLink>
    </div>
    <!--/banner-->
</asp:Content>
<asp:Content ID="body" runat="server" ContentPlaceHolderID="body">
    <div class="wrapper">
        <uc:ucBreadcrumb ID="ctlBreadcrumb" runat="server" />
        <div class="main">
            <div class="content">
                <h2>
                    <asp:Literal ID="litTitle" runat="server" /></h2>
                <div class="listing">
                    <asp:Repeater ID="rptItems" runat="server" OnItemDataBound="rptItems_ItemDataBound">
                        <ItemTemplate>
                            <div class="item">
                                <h4>
                                    <asp:HyperLink ID="lnkTitle" runat="server" /></h4>
                                <p class="intro">
                                    <asp:Literal ID="litSummary" runat="server" /></p>
                                <asp:HyperLink ID="lnkDetails" runat="server" CssClass="btn-text"><span>Resource Details</span></asp:HyperLink>
                            </div>
                            <!--/item-->
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <!--/listing-grid-->
            </div>
            <!--/content-->
        </div>
        <!--/main-->
        <uc:ucCMSMenu ID="ctlCMSMenu" runat="server" />
    </div>
    <!--/wrapper-->
</asp:Content>
