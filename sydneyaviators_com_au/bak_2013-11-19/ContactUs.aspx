﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Main.Master" AutoEventWireup="True"
    CodeBehind="ContactUs.aspx.cs" Inherits="SydneyAviators.Web.ContactUs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls"
    TagPrefix="cc" %>
<%@ Register Src="~/Controls/ucBreadcrumb.ascx" TagName="ucBreadcrumb" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucCMSMenu.ascx" TagName="ucCMSMenu" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">

        function EnquiryType(Id) {
            if (Id == 1) {
                $get("trCourses").style.display = "none";
                $get("trAircrafts").style.display = "none";
            }
            else if (Id == 2) {
                $get("trCourses").style.display = "";
                $get("trAircrafts").style.display = "none";
            }
            else if (Id == 3) {
                $get("trCourses").style.display = "none";
                $get("trAircrafts").style.display = "";
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="banner" runat="server">
    <div class="banner">
        <div class="banner-title">
            <h1>
                Contact Us<span>&nbsp;
                    <asp:Literal ID="litSEOTitle" runat="server" /></span></h1>
        </div>
        <asp:HyperLink ID="lnkEnquiry" runat="server" CssClass="btn-enquiry">Make an enquiry</asp:HyperLink>
    </div>
    <!--/banner-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="wrapper cols3">
        <uc:ucBreadcrumb ID="ctlBreadcrumb" runat="server" />
        <div class="main">
            <div class="content">
                <h2>
                    Make an Enquiry</h2>
                <asp:UpdatePanel ID="udpContactUs" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:PlaceHolder ID="phForm" runat="server">
                         <asp:Literal ID="litSummary" runat="server" />
                             <asp:PlaceHolder ID="phFormInner" runat="server">
                            <div class="form">
                                <h4>
                                    Your Details</h4>
                                <table class="form-table" cellspacing="0">
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="60%" />
                                    </colgroup>
                                    <tr>
                                        <td>
                                            <label>
                                                First Name
                                                <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                                                    ErrorMessage="Please enter your first name" SetFocusOnError="true" EnableClientScript="true"
                                                    validationErrorCssClass="validationError" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                <act:ValidatorCalloutExtender runat="Server" ID="vceFirstName" TargetControlID="rfvFirstName"
                                                    PopupPosition="Right" />
                                            </label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFirstName" runat="server" CssClass="text" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Last Name
                                                <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                                                    ErrorMessage="Please enter your last name" SetFocusOnError="true" EnableClientScript="true"
                                                    validationErrorCssClass="validationError" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                <act:ValidatorCalloutExtender runat="Server" ID="vceLastName" TargetControlID="rfvLastName"
                                                    PopupPosition="Right" />
                                            </label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtLastName" runat="server" CssClass="text" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Email Address
                                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                                    ErrorMessage="Please enter your email" SetFocusOnError="true" EnableClientScript="true"
                                                    validationErrorCssClass="validationError" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                <cc:EmailValidator ID="evEmail" runat="server" ControlToValidate="txtEmail" SetFocusOnError="true"
                                                    ErrorMessage="Invalid email format" Display="Dynamic">*</cc:EmailValidator>
                                                <act:ValidatorCalloutExtender runat="Server" ID="vceEmail" TargetControlID="rfvEmail"
                                                    PopupPosition="Right" />
                                                <act:ValidatorCalloutExtender runat="Server" ID="vceEmailFormat" TargetControlID="evEmail"
                                                    PopupPosition="Right" />
                                            </label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="text" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Contact Telephone</label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtContactTelephone" runat="server" CssClass="text" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--/form-->
                            <div class="form">
                                <h4>
                                    Your Enquiry</h4>
                                <table class="form-table" cellspacing="0">
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="60%" />
                                    </colgroup>
                                    <tr>
                                        <td>
                                            <label>
                                                Enquiring About</label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlEnquiryType" runat="server" DataTextField="EnquiryType"
                                                DataValueField="EnquiryTypeID">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="trCourses" style="display: none;">
                                        <td>
                                            <label>
                                                Courses</label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCourses" runat="server" DataTextField="Title" DataValueField="PageID" />
                                        </td>
                                    </tr>
                                    <tr id="trAircrafts" style="display: none;">
                                        <td>
                                            <label>
                                                Aircrafts</label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlAircrafts" runat="server" DataTextField="Title" DataValueField="PageID" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Comments
                                                <asp:RequiredFieldValidator ID="rfvComments" runat="server" ControlToValidate="txtComments"
                                                    ErrorMessage="Please enter your comments" SetFocusOnError="true" EnableClientScript="true"
                                                    validationErrorCssClass="validationError" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                <act:ValidatorCalloutExtender runat="Server" ID="vceComments" TargetControlID="rfvComments"
                                                    PopupPosition="Right" />
                                            </label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <div class="stacked">
                                                <label>
                                                    <input type="checkbox" />
                                                    Send a copy to me.</label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--/form-->
                            <asp:LinkButton ID="btnSubmit" runat="server" CssClass="btn" OnClick="btnSubmit_Click"> <span>Submit Enquiry</span></asp:LinkButton>
                            <br />
                            <br />
                            <asp:Label ID="lblError" runat="server" ForeColor="Red" />
                            </asp:PlaceHolder>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="phThankYou" runat="server" Visible="false">
                            <p>
                                Thank you for your enquiry. We will be in touch with you shortly.</p>
                        </asp:PlaceHolder>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                  <iframe id="iframeExternalForm" runat="server" class="externalForm" frameborder="0"
                style="width: 100%;" visible="false"></iframe>
            </div>
            <!--/content-->
            

            <div class="sup-content">
                <div class="contact">
                    <h4>
                        Contact Details</h4>
                    <p>
                        <asp:PlaceHolder ID="phPostal" runat="server" Visible="false"><strong>Postal: </strong>
                            <asp:Literal ID="litPostal" runat="server" /><br />
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="phAddress" runat="server" Visible="false"><strong>Address: </strong>
                            <asp:Literal ID="litAddress" runat="server" /><br />
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="phPhone" runat="server" Visible="false"><strong>Contact Number:
                        </strong>
                            <asp:Literal ID="litPhone" runat="server" /><br />
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="phFax" runat="server" Visible="false"><strong>Fax: </strong>
                            <asp:Literal ID="litFax" runat="server" /><br />
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="phEmail" runat="server" Visible="false"><strong>Email: </strong>
                            <asp:HyperLink ID="lnkEmailFromAddress" runat="server" Text="Send us an
                            email" /></asp:PlaceHolder>
                    </p>
                </div>
            </div>
            <!--/sup-content-->
        </div>
        <!--/main-->
    </div>
    <!--/wrapper-->
</asp:Content>
