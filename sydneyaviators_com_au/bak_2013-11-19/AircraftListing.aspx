﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/Main.Master" AutoEventWireup="true"
    CodeBehind="AircraftListing.aspx.cs" Inherits="SydneyAviators.Web.AircraftListing" %>

<%@ Register Src="~/Controls/ucBreadcrumb.ascx" TagName="ucBreadcrumb" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucCMSMenu.ascx" TagName="ucCMSMenu" TagPrefix="uc" %>
<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">

</asp:Content>
<asp:Content ID="banner" runat="server" ContentPlaceHolderID="banner">
    <div class="banner">
        <div class="banner-title">
            <h1>Aircraft Hire<span>&nbsp;
                    <asp:Literal ID="litSEOTitle" runat="server" /></span></h1>
        </div>
        <asp:HyperLink ID="lnkEnquiry" runat="server" CssClass="btn-enquiry">Make an enquiry</asp:HyperLink>
    </div>
    <!--/banner-->
</asp:Content>
<asp:Content ID="body" runat="server" ContentPlaceHolderID="body">
    <div class="wrapper">
        <uc:ucBreadcrumb ID="ctlBreadcrumb" runat="server" />
        <div class="main">
            <div class="content">
                <h2>
                    <asp:Literal ID="litTitle" runat="server" /></h2>
                <div class="listing listing-sup">
                    <asp:Repeater ID="rptItems" runat="server" OnItemDataBound="rptItems_ItemDataBound">
                        <ItemTemplate>
                            <div class="item">
                                <div class="img"><asp:HyperLink ID="lnkThumbnail" runat="server"><asp:Image ID="imgThumbnail" runat="server" CssClass="thumb" Width="140px" Height="140px" /></asp:HyperLink></div>
                                <div class="copy">
                                    <h4><asp:HyperLink ID="lnkTitle" runat="server" /></h4>
                                    <p class="intro">
                                        <asp:Literal ID="litSummary" runat="server" /></p>
                                    <asp:HyperLink ID="lnkDetails" runat="server" CssClass="btn-text"><span>More Details</span></asp:HyperLink>
                                </div>
                                <asp:Panel ID="pnlBookNow" runat="server" CssClass="sub-copy" Visible="false">
                                    <div class="txt">
                                        <p class="rate">
                                            <span class="sml-title">From</span> <strong><asp:Literal ID="litRate" runat="server" /></strong>
                                            Per hour</p>
                                        <p>
                                            Additional fees and services may apply.</p>
                                    </div>
                                    <asp:HyperLink ID="btnBookNow" runat="server" CssClass="btn"><span>Book Now</span></asp:HyperLink>
     
                                </asp:Panel>
                            </div>
                            <!--/item-->
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <!--/listing-aircraft-->
            </div>
            <!--/content-->
        </div>
        <!--/main-->
        <uc:ucCMSMenu ID="ctlCMSMenu" runat="server" />
    </div>
    <!--/wrapper-->
</asp:Content>
