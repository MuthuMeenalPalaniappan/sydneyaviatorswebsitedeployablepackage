﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Main.Master" AutoEventWireup="true"
    CodeBehind="BookAircraft.aspx.cs" Inherits="SydneyAviators.Web.BookAircraft" %>

<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls"
    TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ucBreadcrumb.ascx" TagName="ucBreadcrumb" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        //<![CDATA[
        function initialiseGrid() {
            $(".timeHalf").click(function () { onTimeCellClicked(this); });

            if (parseInt(document.getElementById("<%=hidSelectionStartCellNumber.ClientID%>").value) != NaN && parseInt(document.getElementById("<%=hidSelectionStartCellNumber.ClientID%>").value) >= 0) {
                var cellNumber = parseInt(document.getElementById("<%=hidSelectionStartCellNumber.ClientID%>").value);
                var rowNumber = parseInt(document.getElementById("<%=hidSelectionStartRowNumber.ClientID%>").value);
                setSelected(rowNumber, cellNumber);
                setSelectedStartTimeByCellNumber(rowNumber, cellNumber);
            }
            else
                setSelected(getCellNumberByTime($("OPTION:selected", "#<%= tpStartTime.ClientID %>").val()));
        }

        function onTimeCellClicked(cell) {
            var cellNumber = getCellNumberFromClassName(cell.className);
            var rowNumber = getRowNumberFromClassName(cell.className);
            setSelected(rowNumber, cellNumber);
            setSelectedStartTimeByCellNumber(rowNumber, cellNumber);
        }

        function setSelected(rowNumber, cellNumber) {
            var iterations = getSelectedDurationInMinutes() / 30;
            clearSelection();

            if (cellNumber < 0) {
                document.getElementById("<%=hidSelectionStartCellNumber.ClientID%>").value = "";
                document.getElementById("<%=hidSelectionStartRowNumber.ClientID%>").value = "";
                return;
            }

            for (var i = 0; i < iterations; i++) {
                $(".row" + rowNumber + ".cell" + (cellNumber + i)).each(function (i) {
                    if (this.className.indexOf("selected") < 0)
                        this.className = this.className + " selected";
                });
            }

            document.getElementById("<%=hidSelectionStartCellNumber.ClientID%>").value = cellNumber;
            document.getElementById("<%=hidSelectionStartRowNumber.ClientID%>").value = rowNumber;
        }

        function getCellNumberFromClassName(className) {
            var tmp = className.substr(className.indexOf("cell"), className.length - className.indexOf("cell"));

            if (tmp.indexOf(" ") > 0)
                tmp = tmp.substr(0, tmp.indexOf(" "));

            tmp = tmp.substr(4, tmp.length - 4);

            return parseInt(tmp);
        }

        function getRowNumberFromClassName(className) {
            var tmp = className.substr(className.indexOf("row"), className.length - className.indexOf("cell") - 1);

            if (tmp.indexOf(" ") > 0)
                tmp = tmp.substr(0, tmp.indexOf(" "));

            tmp = tmp.substr(3, tmp.length - 3);

            return parseInt(tmp);
        }

        function getCellNumberSelectionStart() {
            var minCellNumber = 999;
            var cellNumber;

            $(".timeHalf.row1").each(function (i) {
                if (this.className.indexOf(" selected") >= 0) {
                    cellNumber = getCellNumberFromClassName(this.className);
                    if (cellNumber < minCellNumber)
                        minCellNumber = cellNumber;
                }
            });

            return minCellNumber;
        }

        function onDurationChanged() {
            setSelected(getCellNumberSelectionStart());
        }

        function onStartTimeChanged() {
            var startTime = $("OPTION:selected", "#<%= tpStartTime.ClientID %>").val();
            var cellNumber = getCellNumberByTime(startTime);

            if (cellNumber >= 0)
                setSelected(getCellNumberByTime(startTime));
            else
                clearSelection();
        }

        function getCellNumberByTime(time) {
            var cellNumber = -1;

            $(".timeHalf.row1").each(function (i) {
                if ($(this).children("input[type='hidden']").val() == time)
                    cellNumber = getCellNumberFromClassName(this.className);
            });
            return cellNumber;
        }

        function getSelectedDurationInMinutes() {
            return document.getElementById("<%=hidSelectedDuration.ClientID%>").value;
            //return $("OPTION:selected", "#<%= ddlDuration.ClientID %>").val();
        }

        function clearSelection() {
            $(".timeHalf").each(function (i) {
                if (this.className.indexOf(" selected") >= 0)
                    this.className = this.className.replace(" selected", "");
            });
        }

        function setSelectedStartTimeByCellNumber(rowNumber, cellNumber) {
            var tp = $find("<%= tpStartTime.ClientID %>");
            if (tp != null) {
                var startTime = $(".timeHalf.row" + rowNumber + ".cell" + cellNumber).children("input[type='hidden']").val();
                //tp.findItemByValue(startTime).select();
                //alert(tp.get_value());
                document.getElementById("<%=hidSelectedTime.ClientID%>").value = startTime;

                tp.set_value(startTime);
                tp.set_text(tp.findItemByValue(startTime).get_text());
            }
            //alert(tp);

            //$("OPTION:contains('" + startTime + "')","#<%= tpStartTime.ClientID %>").attr("selected", "true");
        }

    </script>
</asp:Content>
<asp:Content ID="banner" ContentPlaceHolderID="banner" runat="server">
    <div class="banner">
        <div class="banner-title">
            <h1>
                <asp:Literal ID="litPageTitle" runat="server" />&nbsp;
                <asp:Literal ID="litSEOTitle" runat="server" /></h1>
        </div>
        <asp:HyperLink ID="lnkEnquiry" runat="server" CssClass="btn-enquiry">Make an enquiry</asp:HyperLink>
    </div>
    <!--/banner-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hidSelectionStartCellNumber" runat="server" />
            <asp:HiddenField ID="hidSelectionStartRowNumber" runat="server" />
            <asp:HiddenField ID="hidSelectedDuration" runat="server" />
            <asp:HiddenField ID="hidSelectedTime" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="wrapper cols3">
        <uc:ucBreadcrumb ID="ctlBreadcrumb" runat="server" />
        <div class="main">
            <div class="content">
                <div class="form">
                    <h4>
                        Your Enquiry</h4>
                    <table class="form-table" cellspacing="0">
                        <colgroup>
                            <col width="40%" />
                            <col width="60%" />
                        </colgroup>
                        <tr>
                            <td>
                                <label>
                                    Aircraft Type</label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAircraftType" runat="server" DataTextField="FullName" DataValueField="ResourceTypeID"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlAircraftType_OnSelectedIndexChanged" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Aircrafts</label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAircrafts" runat="server" DataTextField="Title" DataValueField="ResourceID" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="form">
                    <h4>
                        Time &amp; Date</h4>
                    <table class="form-table" cellspacing="0">
                        <colgroup>
                            <col width="40%" />
                            <col width="60%" />
                        </colgroup>
                        <tr>
                            <td>
                                <label>
                                    Meeting Date
                                </label>
                            </td>
                            <td>
                                <div class="inputBG">
                                    <cc1:DatePicker ID="dpBookingDate" runat="server" AutoPostBack="true" OnSelectedDateChanged="dpBookingDate_SelectedDateChanged" />
                                </div>
                                <div class="clear">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Start Time</label>
                            </td>
                            <td>
                                <cc1:TimePicker ID="tpStartTime" runat="server" CssClass="short" Filter="None" MaxHeight="300px"
                                    Width="100px" AllowCustomText="false" Skin="Premiere" EnableAjaxSkinRendering="true"
                                    EnableEmbeddedSkins="false" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dpStartTime_SelectedIndexChanged" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Meeting Duration</label>
                            </td>
                            <td>
                                <cc1:AutoCompleteDropDownList ID="ddlDuration" CssClass="short" Filter="None" MaxHeight="300px"
                                    Width="100px" AllowCustomText="false" Skin="Premiere" EnableAjaxSkinRendering="true"
                                    EnableEmbeddedSkins="false" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDuration_SelectedIndexChanged">
                                </cc1:AutoCompleteDropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Time Format</label>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnk12HourFormat" runat="server" TransalateIndividualWords="true"
                                    CssClass="timeBTN-L active" OnClick="lnk12HourFormat_OnClick">
                                        12 hr</asp:LinkButton>
                                &nbsp;
                                <asp:LinkButton ID="lnk24HourFormat" runat="server" TransalateIndividualWords="true"
                                    CssClass="timeBTN-R" OnClick="lnk24HourFormat_lnk24HourFormat">24 hr</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--/form-->
                <div class="form">
                    <table width="914" border="0" cellspacing="0" cellpadding="0" class="timeChart-table">
                        <asp:Repeater ID="rptCities" runat="server" OnItemDataBound="rptCities_ItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td id="tdCity" runat="server" class="noBorder">
                                        <h2>
                                            <asp:Literal ID="litAircraft" runat="server" /></h2>
                                        <p>
                                            <asp:Literal ID="litReference" runat="server" /></p>
                                        <p id="pHostType" runat="server" class="hostType">
                                            <asp:Literal ID="litAircraftType" runat="server" /></p>
                                    </td>
                                    <asp:Repeater ID="rptTimeBlock" runat="server">
                                        <ItemTemplate>
                                            <td id="tdTimeBlock" runat="server" class="blue">
                                                <div class="timeHolder">
                                                    <div id="divTimeHalf2" runat="server" class="timeHalf b">
                                                        <asp:HiddenField ID="hidTime2" runat="server" />
                                                    </div>
                                                    <div id="divTimeHalf1" runat="server" class="timeHalf">
                                                        <asp:HiddenField ID="hidTime1" runat="server" />
                                                        <p class="time">
                                                            <asp:Literal ID="litTime" runat="server" />
                                                            <span>
                                                                <asp:Literal ID="litAMPM" runat="server" /></span></p>
                                                    </div>
                                                </div>
                                            </td>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div id="timeChart-bot">
                        <div id="key">
                            <p>
                                <b>
                                    <asp:Literal ID="litKey" runat="server">KEY</asp:Literal></b></p>
                            <p class="key1">
                                <asp:Literal ID="litBusinessHours" runat="server">Business Hours</asp:Literal></p>
                            <p class="key2">
                                <asp:Literal ID="litNonBusinessHours" runat="server">Non-Business Hours</asp:Literal>
                            </p>
                            <p class="key3">
                                <asp:Literal ID="litSelectedTimes" runat="server">Selected Times</asp:Literal>
                            </p>
                        </div>
                        <div id="timeChart-bot-buttons">
                            <asp:LinkButton ID="btnPrevious6Hours" runat="server" Text="Prev 6 Hours" ToolTip="Prev 6 Hours" OnClick="btnPrevious6Hours_Click"
                                CssClass="prev" Visible="false" />
                            <asp:LinkButton ID="btnNext6Hours" runat="server" Text="Next 6 Hours" ToolTip="Next 6 Hours" OnClick="btnNext6Hours_Click"
                                CssClass="next" Visible="false" />
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </div>
                <!--/sup-content-->
            </div>
            <!--/main-->
        </div>
        <!--/wrapper-->
</asp:Content>
