<%@ Page Language="C#" %>
<%@ Register TagPrefix="ed" Namespace="OboutInc.Editor" Assembly="obout_Editor" %>
<script runat="server">

OboutInc.Editor.FieldsFiller plain_filler;

void Page_Load(object o, EventArgs e)
{
  plain_filler = new OboutInc.Editor.FieldsFiller(Page,"paste-plain",Page.Request["localization_path"],Page.Request["language"]);
}
</script>
<html>
<head runat="server">
</head>
<body>
<label>&nbsp;<%= plain_filler.Get("text","Use CTRL+V on your keyboard to paste the text into the window") %></label><br>
<textarea id=area></textarea>
<div style='margin-top:2px;'>
<span class='box' style='float:left; margin-left:5px;'>
<div id='clear' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= plain_filler.Get("clear","Clear") %></div>
</span>
<span class='box' style='float:right; margin-right:5px;'>
<div id='cancel' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= plain_filler.Get("cancel","Cancel") %></div>
</span>
<span class='box' style='float:right; margin-right:5px;'>
<div id='ok' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= plain_filler.Get("insert","Insert") %></div>
</span>
</body>
</html>
