<%@ Page Language="C#" %>
<%@ Register TagPrefix="ed" Namespace="OboutInc.Editor" Assembly="obout_Editor" %>
<script runat="server">

OboutInc.Editor.FieldsFiller filler;

void Page_Load(object o, EventArgs e)
{
  filler = new OboutInc.Editor.FieldsFiller(Page,Page.Request["popup_name"],Page.Request["localization_path"],Page.Request["language"]);
}
</script>
<html>
<head runat="server">
</head>
<body>
<table cellSpacing=0 cellPadding=4 border="0" style="width:100%;height:100%;margin:0px;">
<tr>
<td id="iframePlace" align="right" style="height:100%;width:100%;">
</td>
</tr>
<tr>
<td align="right" style="width:100%">
<div>
<span class='box' style='float:right;'>
<div id='cancel' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= filler.Get("cancel","Cancel") %></div>
</span>
<span class='box' style='float:right; margin-right:5px;'>
<div id='ok' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= filler.Get("ok","OK") %></div>
</span>
</div>
</td>
</tr>
</table>
</body>
</html>