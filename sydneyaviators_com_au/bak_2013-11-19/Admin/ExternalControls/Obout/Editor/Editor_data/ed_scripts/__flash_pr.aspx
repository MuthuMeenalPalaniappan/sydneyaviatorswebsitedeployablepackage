<%@ Page Language="C#" %>
<%@ Register TagPrefix="ed" Namespace="OboutInc.Editor" Assembly="obout_Editor" %>
<script runat="server">

OboutInc.Editor.FieldsFiller flash_filler;

void Page_Load(object o, EventArgs e)
{
  flash_filler = new OboutInc.Editor.FieldsFiller(Page,"flash",Page.Request["localization_path"],Page.Request["language"]);
}
</script>
<html>
<head runat="server">
</head>
<body>
<table border=0 cellspacing=0 cellpadding=0 style="margin: 2px; padding: 0px; width: 99%;">
<tr>
<td>
<table border=0 cellspacing=2 cellpadding=0 style="margin: 2px; padding: 0px; width: 100%;">
<tr>
<td align=left nowrap>
<%= flash_filler.Get("url","Flash URL") %>:
</td>
<td>
<INPUT ID=txtFileName type=text size=40>
</td>
<td align="left" nowrap>
<span class='box' style='float:right; margin-right:7px;'>
<div title="<%= flash_filler.Get("browse-title","Browse Flashes") %>" id='browsButton' style="font-size:7pt;padding-top:4px;width:48px;height:15px;width:expression('50px');height:expression('21px');background-image: url(ed_ab_button.gif);text-align:center;font-weight:bold;" class='button'><%= flash_filler.Get("browse","Browse") %></div>
</span>
</td>
</tr>
<tr>
<td colspan=3 align=left nowrap vAlign="middle">
<%= flash_filler.Get("loop","Loop") %>:
<input id='loop' style='margin:0px; padding:0px;vertical-align:middle;' type='checkbox'/>
<%= flash_filler.Get("autoplay","Autoplay") %>:
<input id='autoplay' style='margin:0px; padding:0px;vertical-align:middle;' type='checkbox'/>
<%= flash_filler.Get("transparency","Transparency") %>:
<input id='transparency' style='margin:0px; padding:0px;vertical-align:middle;' type='checkbox'/>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table border=0 cellspacing=2 cellpadding=0 style="margin: 2px; padding: 0px; width: 100%">
<tr>
<td valign=top style="">
<FIELDSET style="padding: 0px;">
<LEGEND><%= flash_filler.Get("properties","Main properties") %></LEGEND>
<table border=0 cellspacing=2 cellpadding=0 style="margin: 2px; padding: 0px;">
<tr>
<td>
<%= flash_filler.Get("width","Width") %>:
</td>
<td>
<INPUT type=text ID=flashWidth  size=3 style='vertical-align:middle;'>
</td>
</tr>
<tr>
<td>
<%= flash_filler.Get("height","Height") %>:
</td>
<td>
<INPUT type=text ID=flashHeight size=3 style='vertical-align:middle;'>
</td>
</tr>
<tr>
<td nowrap align=left>
<%= flash_filler.Get("background-color","Background Color") %>
</td>
<td nowrap align=left valign=top>
<span style="margin: 0px; padding: 0px;">
<input readonly id="bgColor" type="text" style="margin: 0px; padding: 0px;padding-left:1px; width: 42px; height:17px; cursor: pointer;" value="<%= flash_filler.Get("bg-color-not-set","Not set") %>"
 title="<%= flash_filler.Get("bg-color-popup","Flash background color") %>">
</span>
<span class='box' style='margin:0px; padding:0px;'>
<input id='clearColor' style='margin:0px; padding:0px;margin-left:3px;vertical-align:top;' type='image' src='ed_unformat.gif' class='button' title="<%= flash_filler.Get("remove-bg-color","Remove background color") %>" />
</span>
</td>
</tr>
<tr>
<td>
<%= flash_filler.Get("quality","Quality") %>:
</td>
<td>
<SELECT size=1 ID=selQuality style="margin:0px;">
<OPTION value=high SELECTED> <%= flash_filler.Get("quality-high","High") %> </OPTION>
<OPTION value=medium> <%= flash_filler.Get("quality-medium","Medium") %> </OPTION>
<OPTION value=low> <%= flash_filler.Get("quality-low","Low") %> </OPTION>
</SELECT>
</td>
</tr>
<tr>
<td>
<%= flash_filler.Get("scale","Scale") %>:
</td>
<td>
<SELECT size=1 ID=selScale style="margin:0px;">
<OPTION value="" SELECTED> <%= flash_filler.Get("scale-default","Default") %> </OPTION>
<OPTION value=noborder> <%= flash_filler.Get("scale-noborder","No border") %> </OPTION>
<OPTION value=exactfit> <%= flash_filler.Get("scale-exactfit","Exact fit") %> </OPTION>
</SELECT>
</td>
</tr>
</table>
</FIELDSET>
</td>
<td valign=top style="">
<FIELDSET style="padding: 0px;margin-right:2px;">
<LEGEND><%= flash_filler.Get("layout","Layout") %></LEGEND>
<table border=0 cellspacing=2 cellpadding=0 style="margin: 2px; padding: 0px;">
<tr>
<td>
<%= flash_filler.Get("alignment","Alignment") %>:
</td>
<td>
<SELECT size=1 ID=selAlignment style="margin:0px;">
<OPTION value=""> <%= flash_filler.Get("align-not-set","Not set") %> </OPTION>
<OPTION value=left> <%= flash_filler.Get("align-left","Left") %> </OPTION>
<OPTION value=right> <%= flash_filler.Get("align-right","Right") %> </OPTION>
<OPTION value=texttop> <%= flash_filler.Get("align-texttop","Texttop") %> </OPTION>
<OPTION value=absmiddle> <%= flash_filler.Get("align-absmiddle","Absmiddle") %> </OPTION>
<OPTION value=baseline SELECTED> <%= flash_filler.Get("align-baseline","Baseline") %> </OPTION>
<OPTION value=absbottom> <%= flash_filler.Get("align-absbottom","Absbottom") %> </OPTION>
<OPTION value=bottom> <%= flash_filler.Get("align-bottom","Bottom") %> </OPTION>
<OPTION value=middle> <%= flash_filler.Get("align-middle","Middle") %> </OPTION>
<OPTION value=top> <%= flash_filler.Get("align-top","Top") %> </OPTION>
</SELECT>
</td>
</tr>
<tr>
<td>
<%= flash_filler.Get("horizontal","Horizontal") %>:
</td>
<td>
<INPUT ID=txtHorizontal type=text size=3 maxlength=3 value=""  style="margin:0px;">
</td>
</tr>
<tr>
<td>
<%= flash_filler.Get("vertical","Vertical") %>:
</td>
<td>
<INPUT ID=txtVertical type=text size=3 maxlength=3 value="">
</td>
</tr>
<tr>
<td colspan=2>
<SELECT size=1 style="visibility:hidden">
<OPTION value="" SELECTED>xx</OPTION>
</SELECT>
</td>
</tr>
<tr>
<td colspan=2>
<SELECT size=1 style="visibility:hidden">
<OPTION value="" SELECTED>xx</OPTION>
</SELECT>
</td>
</tr>
</table>
</FIELDSET>
</td>
</tr>
</table>
</td>
</tr>
</table>

<div style='margin-top:2px;'>
<span class='box' style='float:right; margin-right:5px;'>
<div id='cancel' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= flash_filler.Get("cancel","Cancel") %></div>
</span>
<span class='box' style='float:right; margin-right:5px;'>
<div id='ok' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= flash_filler.Get("ok","OK") %></div>
</span>
</div>
</body>
</html>