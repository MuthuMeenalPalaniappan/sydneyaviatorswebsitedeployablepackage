<%@ Page Language="C#" maintainScrollPositionOnPostBack="false" %>
<%@ Register TagPrefix="ed" Namespace="OboutInc.Editor" Assembly="obout_Editor" %>
<html>
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<script runat="server">
void Page_Load(object o, EventArgs e)
{
  Page.Server.ScriptTimeout=2*60;
}
</script>
<%= OboutInc.Editor.Library.SpellHTML(Page,Page.Request["dictionaries_path"],Page.Request["dictionaries"],Page.Request["diclangs"],Page.Request["maxsugg"]) %>
</html>