<%@ Page Language="C#" %>
<%@ Register TagPrefix="ed" Namespace="OboutInc.Editor" Assembly="obout_Editor" %>
<script runat="server">

OboutInc.Editor.FieldsFiller image_filler;

void Page_Load(object o, EventArgs e)
{
  image_filler = new OboutInc.Editor.FieldsFiller(Page,"image",Page.Request["localization_path"],Page.Request["language"]);
}
</script>
<html>
<head runat="server">
</head>
<body>
<table border=0 cellspacing=0 cellpadding=0 style="margin: 2px; padding: 0px; width: 99%;">
<tr>
<td>
<table border=0 cellspacing=2 cellpadding=0 style="margin: 2px; padding: 0px; width: 100%;">
<tr>
<td nowrap>
<%= image_filler.Get("url","Image URL") %>:
</td>
<td>
<INPUT ID=txtFileName type=text size=40>
</td>
<td align="left" nowrap>
<span class='box' style='float:right; margin-right:7px;'>
<!-- <input id='browsButton' type='image' src='ed_browse.gif' class='button' /> -->
<div title="<%= image_filler.Get("browse-title","Browse Images") %>" id='browsButton' style="font-size:7pt;padding-top:4px;width:48px;height:15px;width:expression('50px');height:expression('21px');background-image: url(ed_ab_button.gif);text-align:center;font-weight:bold;" class='button'><%= image_filler.Get("browse","Browse") %></div>
</span>
</td>
</tr>
<tr>
<td nowrap>
<%= image_filler.Get("alt-text","Alternate Text") %>:
</td>
<td colspan=2>
<INPUT type=text ID=txtAltText size=40>
</td>
</tr>
<tr>
<td nowrap vAlign="middle">
<%= image_filler.Get("show-shadow","Show Shadow") %>:
</td>
<td align=left nowrap vAlign="middle">
<input id='showShadow' style='margin:0px; padding:0px;vertical-align:middle;' type='checkbox' title="<%= image_filler.Get("shadow-title","Set/reset shadow effect") %>" />
<%= image_filler.Get("width","Width") %>:
<INPUT type=text ID=imgWidth  size=3 style='vertical-align:middle;'>
<%= image_filler.Get("height","Height") %>:
<INPUT type=text ID=imgHeight size=3 style='vertical-align:middle;'>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table border=0 cellspacing=2 cellpadding=0 style="margin: 2px; padding: 0px; width: 100%">
<tr>
<td valign=top style="height: 99%">
<FIELDSET style="padding: 0px;height:90%;">
<LEGEND><%= image_filler.Get("layout","Layout") %></LEGEND>
<table border=0 cellspacing=2 cellpadding=0 style="margin: 2px; padding: 0px;">
<tr>
<td>
<%= image_filler.Get("alignment","Alignment") %>:
</td>
<td>
<SELECT size=1 ID=selAlignment style="margin:0px;">
<OPTION id=optNotSet value=""> <%= image_filler.Get("align-not-set","Not set") %> </OPTION>
<OPTION id=optLeft value=left> <%= image_filler.Get("align-left","Left") %> </OPTION>
<OPTION id=optRight value=right> <%= image_filler.Get("align-right","Right") %> </OPTION>
<OPTION id=optTexttop value=texttop> <%= image_filler.Get("align-texttop","Texttop") %> </OPTION>
<OPTION id=optAbsMiddle value=absmiddle> <%= image_filler.Get("align-absmiddle","Absmiddle") %> </OPTION>
<OPTION id=optBaseline value=baseline SELECTED> <%= image_filler.Get("align-baseline","Baseline") %> </OPTION>
<OPTION id=optAbsBottom value=absbottom> <%= image_filler.Get("align-absbottom","Absbottom") %> </OPTION>
<OPTION id=optBottom value=bottom> <%= image_filler.Get("align-bottom","Bottom") %> </OPTION>
<OPTION id=optMiddle value=middle> <%= image_filler.Get("align-middle","Middle") %> </OPTION>
<OPTION id=optTop value=top> <%= image_filler.Get("align-top","Top") %> </OPTION>
</SELECT>
</td>
</tr>
<tr>
<td nowrap>
<%= image_filler.Get("border-width","Border Thickness") %>:
</td>
<td>
<INPUT ID=txtBorder type=text size=3 maxlength=3 value="" style="_margin-bottom:2px;">
</td>
</tr>
</table>
</FIELDSET>
</td>
<td valign=top style="height: 99%">
<FIELDSET style="padding: 0px;height:90%;margin-right:2px;">
<LEGEND><%= image_filler.Get("spacing","Spacing") %></LEGEND>
<table border=0 cellspacing=2 cellpadding=0 style="margin: 2px; padding: 0px;">
<tr>
<td>
<%= image_filler.Get("horizontal","Horizontal") %>:
</td>
<td>
<INPUT ID=txtHorizontal type=text size=3 maxlength=3 value=""  style="margin:0px;">
</td>
</tr>
<tr>
<td>
<%= image_filler.Get("vertical","Vertical") %>:
</td>
<td>
<INPUT ID=txtVertical type=text size=3 maxlength=3 value="">
</td>
</tr>
</table>
</FIELDSET>
</td>
</tr>
</table>
</td>
</tr>
</table>

<div style='margin-top:2px;'>
<span class='box' style='float:right; margin-right:5px;'>
<div id='cancel' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= image_filler.Get("cancel","Cancel") %></div>
</span>
<span class='box' style='float:right; margin-right:5px;'>
<div id='ok' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= image_filler.Get("ok","OK") %></div>
</span>
</div>
</body>
</html>