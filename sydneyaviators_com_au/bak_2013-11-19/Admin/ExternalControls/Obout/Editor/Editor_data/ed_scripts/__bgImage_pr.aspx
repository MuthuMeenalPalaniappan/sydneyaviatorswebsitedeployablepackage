<%@ Page Language="C#" %>
<%@ Register TagPrefix="ed" Namespace="OboutInc.Editor" Assembly="obout_Editor" %>
<script runat="server">

OboutInc.Editor.FieldsFiller image_filler;

void Page_Load(object o, EventArgs e)
{
  image_filler = new OboutInc.Editor.FieldsFiller(Page,"background-image",Page.Request["localization_path"],Page.Request["language"]);
}
</script>
<html>
<head runat="server">
</head>
<body>
<table border=0 cellspacing=0 cellpadding=0 style="margin: 2px; padding: 0px; width: 99%;">
<tr>
<td>
<table border=0 cellspacing=2 cellpadding=0 style="margin: 2px; padding: 0px; width: 100%;">
<tr>
<td nowrap>
<%= image_filler.Get("url","Image URL") %>:
</td>
<td>
<INPUT ID=txtFileName type=text size=40>
</td>
<td align="left" nowrap>
<span class='box' style='float:right; margin-right:7px;'>
<div title="<%= image_filler.Get("browse-title","Browse Images") %>" id='browsButton' style="font-size:7pt;padding-top:4px;width:48px;height:15px;width:expression('50px');height:expression('21px');background-image: url(ed_ab_button.gif);text-align:center;font-weight:bold;" class='button'><%= image_filler.Get("browse","Browse") %></div>
</span>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<FIELDSET style="padding: 0px;height:expression('99%');">
<table border=0 cellspacing=2 cellpadding=0 style="margin: 2px; padding: 0px; width: 90%; height:90%">
<tr>
<td valign=top style="height: 85%">
<table border=0 cellspacing=2 cellpadding=0 style="margin: 2px; padding: 0px;">
<tr>
<td>
<%= image_filler.Get("repeat","Repeat") %>:
</td>
<td>
<SELECT size=1 ID=selRepeat style="margin:0px;">
<OPTION value="REPEAT"> <%= image_filler.Get("repeat-all","REPEAT") %> </OPTION>
<OPTION value="REPEAT-X"> <%= image_filler.Get("repeat-x","REPEAT-X") %> </OPTION>
<OPTION value="REPEAT-Y"> <%= image_filler.Get("repeat-y","REPEAT-Y") %> </OPTION>
<OPTION value="NO-REPEAT"> <%= image_filler.Get("repeat-no","NO-REPEAT") %> </OPTION>
</SELECT>
</td>
</tr>
<tr>
<td nowrap>
<%= image_filler.Get("attachment","Attachment") %>:
</td>
<td>
<SELECT size=1 ID=selAttachment style="margin:0px;">
<OPTION value="FIXED"> <%= image_filler.Get("attachment-fixed","FIXED") %> </OPTION>
<OPTION value="SCROLL"> <%= image_filler.Get("attachment-scroll","SCROLL") %> </OPTION>
</SELECT>
</td>
</tr>
</table>
</td>
<td valign=top style="height: 85%">
<table border=0 cellspacing=2 cellpadding=0 style="margin: 2px; padding: 0px;">
<tr>
<td nowrap>
<%= image_filler.Get("position-hor","Position left") %>:
</td>
<td>
<SELECT size=1 ID=selPositionLeft style="margin:0px;">
<OPTION value="LEFT" selected> <%= image_filler.Get("position-hor-left","LEFT") %> </OPTION>
<OPTION value="CENTER"> <%= image_filler.Get("position-hor-center","CENTER") %> </OPTION>
<OPTION value="RIGHT"> <%= image_filler.Get("position-hor-right","RIGHT") %> </OPTION>
</SELECT>
</td>
</tr>
<tr>
<td nowrap>
<%= image_filler.Get("position-vert","Position top") %>:
</td>
<td>
<SELECT size=1 ID=selPositionTop style="margin:0px;">
<OPTION value="TOP" selected> <%= image_filler.Get("position-vert-top","TOP") %> </OPTION>
<OPTION value="CENTER"> <%= image_filler.Get("position-vert-center","CENTER") %> </OPTION>
<OPTION value="BOTTOM"> <%= image_filler.Get("position-vert-bottom","BOTTOM") %> </OPTION>
</SELECT>
</td>
</tr>
</table>
</td>
</tr>
</table>
</FIELDSET>
</td>
</tr>
</table>

<div style='margin-top:2px;'>
<span class='box' style='float:right; margin-right:5px;'>
<div id='cancel' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= image_filler.Get("cancel","Cancel") %></div>
</span>
<span class='box' style='float:right; margin-right:5px;'>
<div id='ok' style="padding-top:5px;width:78px;height:18px;width:expression('80px');height:expression('25px');background-image: url(ed_submit_button.gif);text-align:center;font-weight:bold;" class='button'><%= image_filler.Get("ok","OK") %></div>
</span>
</div>
</body>
</html>