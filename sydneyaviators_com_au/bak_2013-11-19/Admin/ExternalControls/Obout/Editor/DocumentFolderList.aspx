<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentFolderList.aspx.cs" Inherits="HPM.WebApp.Admin.Obout.Editor.DocumentFolderList" %>

<html>
	<head runat="server"></head>
	<body style="padding:0px;margin:0px">
		<div style="padding-left:10px;padding-top:10px;padding-right:10px;"> 
			<asp:Literal id="TreeView" EnableViewState="false" runat="server" />
		</div>
	</body>
</html>	

<script language="javascript" type="text/javascript">
	//document.getElementById('a0_2').onclick();
	
	// implement the OnNodeSelect event that will request the information from the server
	// we redeclared the ob_OnNodeSelect function here - the other option would have been to edit
	// the ob_events_xxxx.js file located in the Script folder
	function ob_OnNodeSelect(id)
	{   
	    if(ob_ev("OnNodeSelect"))
		{	 
		    // load the specified category of emails
			window.parent.DocumentListLoad(id);
		}
	}
</script>