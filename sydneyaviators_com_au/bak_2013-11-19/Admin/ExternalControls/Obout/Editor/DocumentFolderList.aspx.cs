using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using obout_ASPTreeView_2_NET;

namespace HPM.WebApp.Admin.Obout.Editor
{
    public partial class DocumentFolderList : OboutInc.oboutAJAXPage
    {
        protected System.Web.UI.WebControls.Literal TreeView;
        void Page_Load(object sender, EventArgs e)
        {
            string imageFolder = "../../../uploads";
            string folder = System.Web.HttpContext.Current.Server.MapPath(imageFolder);
            string protocol = Page.Request.ServerVariables["SERVER_PROTOCOL"].Split('/')[0].ToLower();
            string port = ":" + Page.Request.ServerVariables["SERVER_PORT"];
            string curPath = protocol + "://" + Page.Request.ServerVariables["SERVER_NAME"] + port + Path.GetDirectoryName(Page.Request.ServerVariables["SCRIPT_NAME"]).Replace("\\", "/") + "/";

            if (port == ":80") port = "";

            // treeview
            obout_ASPTreeView_2_NET.Tree oTree = new obout_ASPTreeView_2_NET.Tree();

            oTree.AddRootNode(string.Empty, null);
            oTree.ShowRootIcon = false;

            oTree.FolderIcons = "../ASPTreeView/tree2/icons";
            oTree.FolderScript = "../ASPTreeView/tree2/script";
            oTree.FolderStyle = "../ASPTreeView/tree2/style/Classic";

            oTree.EditNodeEnable = false;
            oTree.EventList = "OnNodeSelect";

            if (Directory.Exists(folder))
            {
                oTree.Add("root", "myRoot", "<span style=\"cursor:pointer;\">Document Root</span>", true, "ball_redS.gif", null);
                LoadDirectory(oTree, "myRoot", folder, curPath + imageFolder + "/", imageFolder + "/");
            }

            TreeView.Text = oTree.HTML();
        }

        private void LoadDirectory(obout_ASPTreeView_2_NET.Tree oTree, string parentID, string folder, string folderPath, string relPath)
        {
            string[] entires = Directory.GetFileSystemEntries(folder);
            int i = 0;

            foreach (string entire in entires)
            {
                FileAttributes attr = File.GetAttributes(entire);
                string name = Path.GetFileName(entire);
                string pName = Path.GetFileNameWithoutExtension(entire);
                //Path.Getre
                //string title = "";

                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    oTree.Add(parentID, parentID + "_" + pName, "<span style=\"cursor:pointer;\">" + pName + "</span>", false, null, null);
                    LoadDirectory(oTree, parentID + "_" + pName, folder + "/" + name, folderPath + name + "/", relPath + name + "/");
                }
                
                i++;
            }
        }

        private bool ThumbnailCallback()
        {
            return false;
        }
    }
}
