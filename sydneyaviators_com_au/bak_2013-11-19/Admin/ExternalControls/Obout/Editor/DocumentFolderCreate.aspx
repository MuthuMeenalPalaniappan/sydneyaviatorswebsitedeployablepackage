<%@ Page Language="C#" AutoEventWireup="true" Codebehind="DocumentFolderCreate.aspx.cs"
    Inherits="HPM.WebApp.Admin.Obout.Editor.DocumentFolderCreate" %>

<html>
<head>
    <title>Document Library</title>
    <link href="../../AdminStyles.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    function newFolder() {
        document.getElementById('trNewFolder').style.display = "";
        document.getElementById('txtNewFolder').value = "";
    }
    function newFolderCancel() {
        document.getElementById('trNewFolder').style.display = "none";
    }
    </script>
</head>
<body style="padding: 10px;">
    <form id="Form1" runat="server">
        <asp:Label ID="errForm" runat="server"></asp:Label>
        <h1>
            Upload File</h1>
        <br />
        <asp:FileUpload ID="fileDocument" runat="server" CssClass="btn" Style="color: #585956;" />
        <asp:Button ID="btnUpload" runat="server" CssClass="btn" Text="Upload" OnClick="btnUpload_Click" /><br />
        <input type="button" value="New Folder" class="btn" onclick="newFolder();" />
        <asp:Button ID="btnDelete" runat="server" CssClass="btn" Text="Delete Folder" OnClick="btnDelete_Click" />   
        <table border="0">
            <tr id="trNewFolder" style="display: none;">
                <td>
                    <asp:TextBox ID="txtNewFolder" runat="server" CssClass="formField"></asp:TextBox><asp:Button ID="btnCreateFolder" runat="server" CssClass="btn" Text="Create Folder" OnClick="btnCreateFolder_Click" /><input type="button" value="Cancel" class="btn"
                            onclick="newFolderCancel();" />
                </td>
            </tr>
        </table>
        <asp:Literal ID="litRefresh" runat="server"></asp:Literal>
        <div style="padding-top:60px;">
        NOTE: Double click on file to open</div>
    </form>
</body>
</html>
