using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.IO;

namespace HPM.WebApp.Admin.Obout.Editor
{
    public partial class DocumentFolderCreate : System.Web.UI.Page
    {
        string initialDirectory = "../../../uploads";
        string Folder;
    
        void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                errForm.Text = string.Empty;
                litRefresh.Text = string.Empty;
            }

            Folder = Request.QueryString["Folder"];
            //Response.Write(Folder);
            if (Folder != string.Empty)
                Folder = Request.QueryString["Folder"].Replace("myRoot", "_").Replace("_", "/") + "/";
            else
                Folder = "/";

            if (Folder == "myRoot" || Folder == string.Empty)
                btnDelete.Visible = false;
            else
                btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete " + Folder + " and its contents?');");
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string filename = fileDocument.FileName;

            if (filename.Trim() != string.Empty)
            {
                string uploadDir = System.Web.HttpContext.Current.Server.MapPath(initialDirectory + "/" + Folder);

                if (Directory.Exists(uploadDir))
                {
                    try
                    {
                        fileDocument.PostedFile.SaveAs(uploadDir + filename);

                        litRefresh.Text = "<script type=\"text/javascript\">javascript:parent.location.href=\"DocumentLibrary.aspx\";</script>";
                    }
                    catch (Exception ex)
                    {
                        errForm.Text = "Error: " + ex.Message + Environment.NewLine + uploadDir + filename;
                    }
                }
                else
                {
                     errForm.Text = "Error: folder " + uploadDir + " does not exist";
                }
            }
        }

        protected void btnCreateFolder_Click(object sender, EventArgs e)
        {
            string createDir = System.Web.HttpContext.Current.Server.MapPath(initialDirectory + "/" + Folder);

            if (Directory.Exists(createDir))
            {
                try
                {
                    Directory.CreateDirectory(@createDir + txtNewFolder.Text.Replace(" ", ""));

                    litRefresh.Text = "<script type=\"text/javascript\">javascript:parent.location.href=\"DocumentLibrary.aspx\";</script>";
                }
                catch (Exception ex)
                {
                    errForm.Text = "Error: " + ex.Message;
                }
            }
            else
            {
                errForm.Text = "Error: folder " + createDir + " does not exist";
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string deleteDir = System.Web.HttpContext.Current.Server.MapPath(initialDirectory + "/" + Folder);
            
            if (Directory.Exists(deleteDir))
            {
                try
                {
                    //DeleteDirectory(deleteDir);
                    
                    litRefresh.Text = "<script type=\"text/javascript\">javascript:parent.location.href=\"DocumentLibrary.aspx\";</script>";
                }
                catch (Exception ex)
                {
                    errForm.Text = "Error: " + ex.Message;
                }
            }
            else
            {
                errForm.Text = "Error: folder " + deleteDir + " does not exist";
            }
        }

        private void DeleteDirectory(string dir)
        {
            DirectoryInfo parentDir;
            DirectoryInfo[] childDirs;
            FileInfo[] childFiles;

            parentDir = new DirectoryInfo(dir);
            childDirs = parentDir.GetDirectories();
            childFiles = parentDir.GetFiles();

            foreach (DirectoryInfo chDir in childDirs)
            {
                DeleteDirectory(chDir.Name);
            }

            foreach (FileInfo chFil in childFiles)
            {
                //chFil.MoveTo(dir + "../../Library_Deleted");
            }

            Directory.Move(dir, System.Web.HttpContext.Current.Server.MapPath("Uploads_Deleted"));
                    
        }
    }
}
