<%@ Page Language="C#" Inherits="OboutInc.oboutAJAXPage" %>

<%@ Import Namespace="obout_ASPTreeView_2_NET" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>
<%@ Import Namespace="HPM.WebApp" %>
<%@ Register TagPrefix="ogrid" Namespace="Obout.Grid" Assembly="obout_Grid_NET" %>
<%@ Register TagPrefix="oajax" Namespace="OboutInc" Assembly="obout_AJAXPage" %>
<%@ Register TagPrefix="obspl" Namespace="OboutInc.Splitter2" Assembly="obout_Splitter2_Net" %>

<script language="C#" runat="server">
    string initialDirectory = "../../../uploads";
    string DefaultFolder;
    int expandedLevel = 0;

    void Page_Load(object sender, EventArgs e)
    {
        ThrowExceptionsAtClient = false;
        ShowErrorsAtClient = false;

        string Folder = Request.QueryString["Folder"];
        if (Folder != string.Empty)
            LoadGrid(Request.QueryString["Folder"].Replace("_", "\\").Replace("myRoot", string.Empty));
        else
            LoadGrid(Folder);    
    }

    public void LoadGrid(string Folder)
    {
        DataSet dsDir = new DataSet();
        dsDir.Tables.Add(new DataTable());
        dsDir.Tables[0].Columns.Add(new DataColumn("name", System.Type.GetType("System.String")));
        dsDir.Tables[0].Columns.Add(new DataColumn("size", typeof(double)));
        dsDir.Tables[0].Columns.Add(new DataColumn("type", System.Type.GetType("System.String")));
        dsDir.Tables[0].Columns.Add(new DataColumn("datemodified", System.Type.GetType("System.String")));
        dsDir.Tables[0].Columns.Add(new DataColumn("imageType", System.Type.GetType("System.String")));

        DirectoryInfo rootFolder = new DirectoryInfo(Server.MapPath(initialDirectory + Folder));
        hidDir.Value = initialDirectory + Folder;

        foreach (DirectoryInfo dir in rootFolder.GetDirectories())
        {
            string dirName = dir.Name;
            string dirDateTime = dir.LastAccessTime.ToString("d/M/yyyy h:m:s tt");
            string dirImageType = "Folder";
            dsDir.Tables[0].Rows.Add(new object[] { dirName, 0, "File Folder", dirDateTime, dirImageType });
        }

        foreach (FileInfo file in rootFolder.GetFiles())
        {
            string fileName = file.Name;
            double fileSize = (double)file.Length / 1024;
            string fileType = file.Extension.Replace(".", "");
            string fileImageType = "File";
            string fileDateTime = file.LastAccessTime.ToString("d/M/yyyy h:m:s tt");

            if (fileSize > 1)
                fileSize = Math.Round(fileSize, 0);
            else
                fileSize = Math.Round(fileSize, 1);

            dsDir.Tables[0].Rows.Add(new object[] { fileName, fileSize, fileType, fileDateTime, fileImageType });
        }

        gridDir.DataSource = dsDir;
        gridDir.DataBind();
    }
	
</script>

<script type="text/javascript">
function showFile() {
    var filetype = gridDir.SelectedRecords[0].Type;
    //alert(gridDir.SelectedRecords[0].Type);
    //alert(document.getElementById('<%= hidDir.ClientID %>').value);
    if (filetype != "File Folder") {
        var dirID = document.getElementById('<%= hidDir.ClientID %>').value;
        //alert(dirID);
        window.open(dirID.replace(/_/g, "/") + "/" + gridDir.SelectedRecords[0].Name);
    }
}
</script>

<html>
<head runat="server">
    <title>Document Library</title>
    <link href="../../AdminStyles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="Form1" runat="server">
        <div style="width:100%;">
        <input type="hidden" id="hidDir" runat="server" />
        <oajax:CallbackPanel ID="cpDir" runat="server">
            <Content>
                <ogrid:Grid ID="gridDir" runat="server" AllowRecordSelection="true" ShowFooter="false"
                    KeepSelectedRecords="false" AllowAddingRecords="false" CallbackMode="true" Serialize="false"
                    AllowColumnResizing="true" ShowHeader="true" Width="100%" PageSize="100" FolderStyle="../grid/styles/style_5"
                    AutoGenerateColumns="false">
                    <ClientSideEvents OnClientDblClick="showFile" />
                    <Columns>
                        <ogrid:Column ID="Column1" DataField="imageType" HeaderText="" Width="10%" runat="server"
                            TemplateId="tplImageType" />
                        <ogrid:Column ID="Column2" DataField="Name" HeaderText="Name" Width="30%" runat="server" />
                        <ogrid:Column ID="Column3" DataField="Size" HeaderText="Size" Width="15%" runat="server"
                            TemplateId="tplSize" />
                        <ogrid:Column ID="Column4" DataField="Type" HeaderText="Type" Width="15%" runat="server" />
                        <ogrid:Column ID="Column5" DataField="DateModified" HeaderText="Date Modified" Width="20%"
                            runat="server" />
                    </Columns>
                    <Templates>
                        <ogrid:GridTemplate runat="server" ID="tplImageType">
                            <Template>
                                <div style="width: 100%; height: 100%; text-align: right; padding-top: 4px">
                                    <img src="Splitter/images/filebrowser/<%# Container.Value %>.gif" />
                                </div>
                            </Template>
                        </ogrid:GridTemplate>
                        <ogrid:GridTemplate runat="server" ID="tplSize">
                            <Template>
                                <div style="width: 100%; height: 100%; padding-left: 10px; padding-top: 6px">
                                    <%# Container.Value == "0" ? "" : Container.Value + " KB" %>
                                </div>
                            </Template>
                        </ogrid:GridTemplate>
                    </Templates>
                </ogrid:Grid>
            </Content>
            <Loading>
                <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center">
                            <img src="Splitter/images/loading_icons/1.gif">
                        </td>
                    </tr>
                </table>
            </Loading>
        </oajax:CallbackPanel>
        <oajax:CallbackPanel ID="cpLabel" runat="server">
            <Content>
                <asp:Label ID="lblDir" runat="server" />
            </Content>
        </oajax:CallbackPanel>
        </div>
    </form>
</body>
</html>

<script type="text/javascript">
function ob_OnNodeSelect(id)
{   
    document.getElementById('hidDir').value = id;
	document.getElementById('trNewFolder').style.display = "none";
	SelectDir(id);
}

</script>

