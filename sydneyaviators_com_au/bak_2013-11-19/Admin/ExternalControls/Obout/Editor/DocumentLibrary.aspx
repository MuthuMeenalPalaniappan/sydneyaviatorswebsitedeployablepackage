<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentLibrary.aspx.cs" Inherits="HPM.WebApp.Admin.Obout.Editor.DocumentLibrary" %>

<%@ Register TagPrefix="obspl" Namespace="OboutInc.Splitter2" Assembly="obout_Splitter2_Net"%>
<html>
	<head runat="server">
		<title>Document Library</title>
		<style type="text/css">
		.Text
		{
			background-color:white;
			font-size:12px;
		}
		.inputButton
		{
			border:1px solid #6B89AF;
			width:250px;
		}
		body
		{
			font-family:Tahoma;
			font-size:8pt;
		}
		.pointer
		{
			cursor:pointer;
		}
		</style>
	</head>
	<body onload="DocumentListLoad('');">
		<obspl:Splitter CookieDays="0" id="sp1" runat="server" StyleFolder="Splitter/styles/default">
			<LeftPanel WidthMin="100" WidthMax="400" WidthDefault="180">
				<Content Url="DocumentFolderList.aspx" />
			</LeftPanel>
			<RightPanel>
				<Content>
					<obspl:HorizontalSplitter CookieDays="0" id="sp2" runat="server" StyleFolder="styles/default">
						<TopPanel HeightMin="200" HeightMax="200" HeightDefault="200">
							<Content></Content>
						</TopPanel>
						<BottomPanel>
							<Content></Content>
						</BottomPanel>
					</obspl:HorizontalSplitter>
				</Content>
				<Footer Height="0"></Footer>
			</RightPanel>
		</obspl:Splitter>
	</body>
</html>

<script type="text/javascript">

// load, specified by param, an email category
function DocumentListLoad(Folder)
{
    sp2.loadPage("TopContent", "DocumentFolderCreate.aspx?Folder=" + Folder);
    sp2.loadPage("BottomContent", "DocumentList.aspx?Folder=" + Folder);
}

</script>