﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucCMSAircraftListingInfo.ascx.cs" Inherits="SydneyAviators.Web.Admin.CMSAircraft.Controls.ucCMSAircraftListingInfo" %>

<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls" TagPrefix="cc" %>
<%@ Register Src="ucCMSAircraftListingDetails.ascx" TagName="ucDetails" TagPrefix="uc" %>
<%@ Register Src="~/Admin/CMSPage/Controls/ucCMSPageDetails.ascx" TagName="ucPageDetails" TagPrefix="uc" %>
<%@ Register Src="~/Admin/CMSPage/Controls/ucCMSPageSearchList.ascx" TagName="ucPageList" TagPrefix="uc" %>

<div class="form-wrap">
    <asp:UpdatePanel ID="upTabs" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <ul class="tabs">
                <li>
                    <asp:LinkButton ID="lnkDetails" runat="server" CssClass="current" OnClick="lnkDetails_Click">Aircraft Listing Details</asp:LinkButton></li>
                <li>
                    <asp:LinkButton ID="lnkPageDetails" runat="server" OnClick="lnkPageDetails_Click">SEO Details</asp:LinkButton></li>
            </ul>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="pane">
        <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlDetails" runat="server" Visible="true">
                    <uc:ucDetails ID="ctlDetails" runat="server" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upPageDetails" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlPageDetails" runat="server" Visible="false">
                    <uc:ucPageDetails ID="ctlPageDetails" runat="server" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <br />
    <br />
    <asp:Panel ID="pnBottom" runat="server">
        <asp:UpdatePanel ID="upBottomTabs" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="tabs">
                    <li>
                        <asp:LinkButton ID="lnkPages" runat="server" CssClass="current" OnClick="lnkPages_Click">Aircrafts</asp:LinkButton></li>
                </ul>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="pane">
            <asp:UpdatePanel ID="upPages" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlPages" runat="server" Visible="true">
                        <uc:ucPageList ID="ctlPageList" runat="server" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
</div>
