﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucCMSAircraftDetails.ascx.cs" Inherits="SydneyAviators.Web.Admin.CMSAircraft.Controls.ucCMSAircraftDetails" %>

<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls"
    TagPrefix="cc" %>
<%@ Register Src="~/Admin/Controls/ucModuleHeader.ascx" TagName="ucModuleHeader" TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucButton.ascx" TagName="ucButton" TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucMessageBox.ascx" TagName="ucMessage" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucAJAXFileUpload.ascx" TagName="ucAJAXFileUpload" TagPrefix="uc" %>
<div class="module">
    <asp:UpdatePanel ID="upHeader" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc:ucModuleHeader ID="ctlModuleHeader" runat="server" TitleText="Aircraft Details"
                ControlToToggle="" LoadCollapsed="false" ShowEditButton="true" OnEdit="ctlModuleHeader_Edit"
                OnCancel="ctlModuleHeader_Cancel" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="clear">
        &nbsp;</div>
    <div class="pane">
        <p>
            Enter the following details about the aircraft page.</p>
        <div class="form-wrap">
            <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table id="tblContent" runat="server" class="detail-table" border="0" cellpadding="0"
                        cellspacing="0" width="">
                        <tr>
                            <td width="20%">
                                <label>
                                    Title <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle"
                                    ErrorMessage="Please enter a title" SetFocusOnError="true" EnableClientScript="true" validationErrorCssClass="validationError" Display="Dynamic">*</asp:RequiredFieldValidator></label>
                            </td>
                            <td width="30%">
                                <asp:Literal ID="litTitle" runat="server" />
                                <asp:TextBox ID="txtTitle" runat="server" CssClass="text span-4" />
                            </td>
                            <td width="20%"><label>Reference</label>
                            </td>
                            <td width="30%">
                                <asp:Literal ID="litReference" runat="server" />
                                <asp:TextBox ID="txtReference" runat="server" CssClass="text span-4" />
                            </td>
                            </tr>
                            <tr>
                            <td>
                                <label>
                                    Status</label>
                            </td>
                            <td>
                                <asp:Literal ID="litStatus" runat="server" />
                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="select span-4" />
                            </td>
                            <td>
                                <label>
                                    </label>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Solo Rates</label>
                            </td>
                            <td>
                               <asp:Literal ID="litSoloRates" runat="server" />
                                <asp:TextBox ID="txtSoloRates" runat="server" CssClass="text span-4" />
                            </td>
                            <td>
                                <label>
                                    Dual Rates</label>
                            </td>
                            <td>
                               <asp:Literal ID="litDualRates" runat="server" />
                                <asp:TextBox ID="txtDualRates" runat="server" CssClass="text span-4" />
                            </td>
                        </tr>
                         <tr>
                            
                            <td>
                                <label>
                                    Landing Fees</label>
                            </td>
                            <td>
                                <asp:Literal ID="litLandingFees" runat="server" />
                                <asp:TextBox ID="txtLandingFees" runat="server" CssClass="text span-4" />
                            </td>
                            <td><label></label></td>
                            <td></td>
                        </tr>
                         <tr>
                            <td>
                                <label>
                                    Thumbnail</label>
                            </td>
                            <td colspan="3">
                                <uc:ucAJAXFileUpload ID="filThumbnail" runat="server" />
                                <asp:Image ID="imgThumbnail" runat="server" />
                                <br />
                                <asp:HyperLink ID="lnkThumbnail" runat="server" Text="View" Target="_blank" />
                                <asp:LinkButton ID="btnDeleteThumbnail" runat="server" Text="Delete" OnClick="btnDeleteThumbnail_Click" OnClientClick="return confirm('Are you sure you want to delete this?');" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Summary</label>
                            </td>
                            <td colspan="3">
                                <asp:Literal ID="litSummary" runat="server" />
                                <cc:HTMLEditor ID="txtSummary" runat="server" Height="250" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Content</label>
                            </td>
                            <td colspan="3">
                                <asp:Literal ID="litContent" runat="server" />
                                <cc:HTMLEditor ID="txtContent" runat="server" Height="750" />
                            </td>
                        </tr>
                       
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Edit" />
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Cancel" />
                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upButtons" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divButtonBar" runat="server" class="actions" visible="false">
                        <div class="left">
                        </div>
                        <div class="right">
                            <uc:ucbutton id="btnSave" cssclass="btn" text="Save" runat="server" onclick="btnSave_Click" CausesValidation="true" />
                        </div>
                    </div>
                    <uc:ucMessage id="ctlMessage" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnDeleteThumbnail" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Edit" />
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Cancel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
