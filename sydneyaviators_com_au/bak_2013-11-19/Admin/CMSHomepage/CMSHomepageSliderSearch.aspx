﻿<%@ Page Language="C#" MasterPageFile="~/Admin/MasterPage/Admin.Master" AutoEventWireup="True" CodeBehind="CMSHomepageSliderSearch.aspx.cs" Inherits="SydneyAviators.Web.Admin.CMSHomepage.CMSHomepageSliderSearch" %>


<%@ Register Src="~/Admin/Controls/ucTitleBar.ascx" TagName="ucTitleBar" TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucCMSMenu.ascx" TagName="ucCMSMenu" TagPrefix="uc" %>
<%@ Register Src="Controls/ucCMSHomepageSliderSearchFilter.ascx" TagName="ucFilter" TagPrefix="uc" %>
<%@ Register Src="Controls/ucCMSHomepageSliderSearchList.ascx" TagName="ucList" TagPrefix="uc" %>
<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="sidebar" runat="server" ContentPlaceHolderID="sidebar">
 <uc:ucCMSMenu ID="ctlCMSMenu" runat="server" />
</asp:Content>
<asp:Content ID="main" runat="server" ContentPlaceHolderID="main">

<asp:ScriptManager ID="ScriptManagerMain" runat="server" />
    <uc:ucTitleBar ID="ctlTitleBar" runat="server" TitleText="Homepage Promo Images" />
    <uc:ucFilter ID="ctlFilter" runat="server" OnSearch="ctlFilter_Search" OnReset="ctlFilter_Reset"
        runat="server" />
    <uc:ucList ID="ctlList" runat="server" />
</asp:Content>
