﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucCMSHomepageLinkCategoryInfo.ascx.cs" Inherits="SydneyAviators.Web.Admin.CMSHomepage.Controls.ucCMSHomepageLinkCategoryInfo" %>
<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls" TagPrefix="cc" %>
<%@ Register Src="ucCMSHomepageLinkCategoryDetails.ascx" TagName="ucDetails" TagPrefix="uc" %>
<%@ Register Src="ucCMSHomepageLinkSearchList.ascx" TagName="ucLinks" TagPrefix="uc" %>

<div class="form-wrap">
    <asp:UpdatePanel ID="upTabs" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <ul class="tabs">
                <li>
                    <asp:LinkButton ID="lnkDetails" runat="server" CssClass="current" OnClick="lnkDetails_Click">Link Category Details</asp:LinkButton></li>
            </ul>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="pane">
        <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlDetails" runat="server" Visible="true">
                    <uc:ucDetails ID="ctlDetails" runat="server" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <br />
    <br />
     <asp:Panel ID="pnBottom" runat="server">
        <asp:UpdatePanel ID="upBottomTabs" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="tabs">
                 
                            <li>
                        <asp:LinkButton ID="lnkLinks" runat="server" CssClass="current" OnClick="lnkLinks_Click">Links</asp:LinkButton></li>

                </ul>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="pane">


              <asp:UpdatePanel ID="upLinks" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlLinks" runat="server" Visible="true">
                        <uc:ucLinks ID="ctlLinks" runat="server" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
</div>
