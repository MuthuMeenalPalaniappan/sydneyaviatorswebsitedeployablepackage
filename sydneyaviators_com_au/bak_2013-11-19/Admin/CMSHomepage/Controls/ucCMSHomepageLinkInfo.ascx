﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucCMSHomepageLinkInfo.ascx.cs" Inherits="SydneyAviators.Web.Admin.CMSHomepage.Controls.ucCMSHomepageLinkInfo" %>
<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls" TagPrefix="cc" %>
<%@ Register Src="ucCMSHomepageLinkDetails.ascx" TagName="ucDetails" TagPrefix="uc" %>

<div class="form-wrap">
    <asp:UpdatePanel ID="upTabs" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <ul class="tabs">
                <li>
                    <asp:LinkButton ID="lnkDetails" runat="server" CssClass="current" OnClick="lnkDetails_Click">Link Details</asp:LinkButton></li>
            </ul>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="pane">
        <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlDetails" runat="server" Visible="true">
                    <uc:ucDetails ID="ctlDetails" runat="server" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <br />
    <br />
</div>
