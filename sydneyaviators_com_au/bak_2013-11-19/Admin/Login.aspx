﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="Login.aspx.cs" Inherits="SydneyAviators.Web.Admin.Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Sydney Aviators Admin</title>
</head>
<body class="login">
    <div class="module">
        <form id="form1" runat="server">
        <asp:ScriptManager ID="scriptManager" runat="server" />
        <div class="head">
            <p class="title">
                Sydney Aviators Login</p>
        </div>
        <asp:Panel ID="pnlLogin" runat="server" CssClass="pane" DefaultButton="btnLogin">
            <div class="center">
                <asp:Image ID="imgLogo" runat="server" SkinID="imgLogo" /></div>
            <br />
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        Username:<asp:RequiredFieldValidator ID="rfvUsername" runat="server" ControlToValidate="txtUsername"
                            SetFocusOnError="true" ErrorMessage="Please enter your username" Display="None">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtUsername" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Password:<asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                            SetFocusOnError="true" ErrorMessage="Please enter your password" Display="None">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" AutoCompleteType="None" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:ImageButton ID="btnLogin" runat="server" OnClick="btnLogin_Click" SkinID="btnLogin" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblError" runat="server" ForeColor="Red" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        </form>
    </div>
</body>
</html>
