﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucCMSPageDetails.ascx.cs"
    Inherits="SydneyAviators.Web.Admin.CMSPage.Controls.ucCMSPageDetails" %>
<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls"
    TagPrefix="cc" %>
<%@ Register Src="~/Admin/Controls/ucModuleHeader.ascx" TagName="ucModuleHeader"
    TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucButton.ascx" TagName="ucButton" TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucMessageBox.ascx" TagName="ucMessage" TagPrefix="uc" %>
<div class="module">
    <asp:UpdatePanel ID="upHeader" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc:ucModuleHeader ID="ctlModuleHeader" runat="server" TitleText="SEO Details" ControlToToggle=""
                LoadCollapsed="false" ShowEditButton="true" OnEdit="ctlModuleHeader_Edit" OnCancel="ctlModuleHeader_Cancel" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="clear">
        &nbsp;</div>
    <div class="pane">
        <p>
            Enter the following details about the page.</p>
        <div class="form-wrap">
            <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table id="tblContent" runat="server" class="detail-table" border="0" cellpadding="0"
                        cellspacing="0" width="">
                        <tr>  
                            <td width="20%">
                                <label>
                                    Page Title <asp:RequiredFieldValidator ID="rfvPageTitle" runat="server" ControlToValidate="txtPageTitle"
                                        ErrorMessage="Please enter a page title" SetFocusOnError="true" EnableClientScript="true"
                                        validationErrorCssClass="validationError" Display="Dynamic">*</asp:RequiredFieldValidator></label>
                            </td>
                            <td width="30%">
                                <asp:Literal ID="litPageTitle" runat="server" />
                                <asp:TextBox ID="txtPageTitle" runat="server" CssClass="text span-4" />
                            </td>
                           <td width="20%">
                                <label>
                                    SEO Title</label>
                            </td>
                           <td width="30%">
                                <asp:Literal ID="litSEOTitle" runat="server" />
                                <asp:TextBox ID="txtSEOTitle" runat="server" CssClass="text span-4" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Page URL</label>
                            </td>
                            <td>
                                <asp:Literal ID="litPageUrl" runat="server" />
                                <asp:TextBox ID="txtPageUrl" runat="server" CssClass="text span-4" />
                            </td>    
                            <td>
                            <label></label></td>    
                            <td></td>  
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Meta Title</label>
                            </td>
                            <td>
                                <asp:Literal ID="litMetaTitle" runat="server" />
                                <asp:TextBox ID="txtMetaTitle" runat="server" CssClass="text span-4" />
                            </td>
                            <td>
                                <label>
                                    Meta Keywords</label>
                            </td>
                            <td>
                                <asp:Literal ID="litMetaKeywords" runat="server" />
                                <asp:TextBox ID="txtMetaKeywords" runat="server" CssClass="text span-4" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Meta Description</label>
                            </td>
                            <td>
                                <asp:Literal ID="litMetaDescription" runat="server" />
                                <asp:TextBox ID="txtMetaDescription" runat="server" TextMode="MultiLine" Rows="3"
                                    CssClass="text span-4" />
                            </td>
                            <td><label></label></td>
                            <td></td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Edit" />
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Cancel" />
                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upButtons" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divButtonBar" runat="server" class="actions" visible="false">
                        <div class="left">
                        </div>
                        <div class="right">
                            <uc:ucButton ID="btnSave" CssClass="btn" Text="Save" runat="server" OnClick="btnSave_Click"
                                CausesValidation="true" />
                        </div>
                    </div>
                    <uc:ucMessage ID="ctlMessage" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Edit" />
                    <asp:AsyncPostBackTrigger ControlID="ctlModuleHeader" EventName="Cancel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
