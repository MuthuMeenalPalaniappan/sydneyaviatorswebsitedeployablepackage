﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucModuleHeader.ascx.cs" Inherits="SydneyAviators.Web.Admin.Controls.ucModuleHeader" %>

<div class="head">
    <asp:LinkButton ID="btnToggle" CssClass="handle" OnClick="btnToggle_Click" runat="server"></asp:LinkButton>
    <p class="title">
        <asp:Literal ID="litTitleText" runat="server"></asp:Literal></p>
    <p class="btns">
        <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn" Visible="false" CausesValidation="false" OnClick="btnCancel_Click"><span>Cancel</span></asp:LinkButton>
        <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn" Visible="false" CausesValidation="false" OnClick="btnEdit_Click"><span>Edit</span></asp:LinkButton>
    </p>
</div>
<!-- /.head -->
<div class="clear">
    &nbsp;</div>
