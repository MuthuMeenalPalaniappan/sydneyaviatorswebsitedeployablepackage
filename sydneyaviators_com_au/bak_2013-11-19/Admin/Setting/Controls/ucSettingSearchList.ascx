﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucSettingSearchList.ascx.cs" Inherits="SydneyAviators.Web.Admin.Setting.Controls.ucSettingSearchList" %>
<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls"
    TagPrefix="cc" %>
<%@ Register Src="~/Admin/Controls/ucModuleHeader.ascx" TagName="ucModuleHeader" TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucPager.ascx" TagName="ucPager" TagPrefix="uc" %>
<div class="module">
    <uc:ucModuleHeader ID="moduleHeaderResults" ControlToToggle="pnlSearchResults" TitleText="Settings"
        runat="server" />
    <div class="clear">
        &nbsp;</div>
    <asp:Panel ID="pnlSearchResults" CssClass="pane" runat="server">
        <div class="listing-wrap">
            <asp:UpdatePanel ID="upGrid" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <cc:CustomGridView2 ID="grdItems" runat="server" CssClass="listing-table" OnRowDataBound="grdItems_RowDataBound"
                        CellSpacing="0" OnDataBound="grdItems_DataBound">
                        <AlternatingRowStyle CssClass="alt-row" />
                        <Columns>
                            <asp:TemplateField HeaderText="Setting" HeaderStyle-Width="35%" SortExpression="Setting">
                                <ItemTemplate>
                                    <a title='<%# Eval("Description")%>' class="info"><asp:Image ID="imgInfo" runat="server" SkinID="imgInfo" /></a>&nbsp;&nbsp;<%# Eval("Setting")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Values" HeaderStyle-Width="35%" SortExpression="Value">
                                <ItemTemplate>
                                   <asp:Label ID="lblValue" runat="server" Text='<%# Eval("Value")%>' />
                                   <asp:TextBox ID="txtValue" runat="server" Text='<%# Eval("Value")%>' CssClass="text" Visible="false" />
                                   <asp:HiddenField ID="hidEditable" runat="server" Value='<%# Eval("Editable")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" HeaderStyle-Width="15%" SortExpression="StatusID">
                                <ItemTemplate>
                                    <%# ((SydneyAviators.Business.Enums.Status)(Enum.Parse(typeof(SydneyAviators.Business.Enums.Status), Eval("StatusID").ToString()))).ToString()%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <center>
                                        <asp:LinkButton ID="btnEdit" CssClass="btn" runat="server" OnClick="btnEdit_Click"><span>Edit</span></asp:LinkButton>
                                        <asp:LinkButton ID="btnSave" CssClass="btn" runat="server" OnClick="btnSave_Click" Visible="false" OnClientClick="return confirm('Are you sure you want to update this? New settings will be effective when the current cache is timed out.');"><span>Save</span></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" CssClass="btn" runat="server" OnClick="btnCancel_Click" Visible="false"><span>Cancel</span></asp:LinkButton></center> 
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </cc:CustomGridView2>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ctlPager" EventName="PagerClick" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upPager" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <uc:ucPager ID="ctlPager" runat="server" OnPagerClick="ctlPager_PagerClick" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
</div>
