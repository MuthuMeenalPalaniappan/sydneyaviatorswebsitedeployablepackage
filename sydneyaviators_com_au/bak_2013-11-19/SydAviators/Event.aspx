﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/Main.Master" AutoEventWireup="true"
    CodeBehind="Event.aspx.cs" Inherits="SydneyAviators.Web.Event" %>

<%@ Register Src="~/Controls/ucBreadcrumb.ascx" TagName="ucBreadcrumb" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucCMSMenu.ascx" TagName="ucCMSMenu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucFacebookComments.ascx" TagName="ucFacebookComments" TagPrefix="uc" %>

<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="banner" runat="server">
    <div class="banner">
        <div class="banner-title">
      <h1>Events<span>&nbsp;
                    <asp:Literal ID="litSEOTitle" runat="server" /></span></h1>
        </div>
        <asp:HyperLink ID="lnkEnquiry" runat="server" CssClass="btn-enquiry">Make an enquiry</asp:HyperLink>
    </div>
    <!--/banner-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="wrapper cols3">
        <uc:ucBreadcrumb ID="ctlBreadcrumb" runat="server" />
        <div class="main">
            <div class="content">
                <h2>
                    Events</h2>
                <asp:Image ID="imgThumbnail" runat="server" CssClass="thumb left" />
                <div class="copy">
                    <span class="sml-title">
                        <asp:Literal ID="litDate" runat="server" /></span>
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" /></h4>
                </div>
                <asp:Literal ID="litContent" runat="server" />
                 <uc:ucFacebookComments ID="ctlFacebookComments" runat="server" />
            </div>
            <!--/content-->
            <div class="sup-content">
                <div class="item">
                    <p>
                        Click below to register your interest in this event.</p>
                    <asp:HyperLink ID="lnkRegister" runat="server" CssClass="btn2"><span>Register</span></asp:HyperLink>
                </div>
                <!--/item-->
            </div>
            <!--/sup-content-->
        </div>
        <!--/main-->
        <uc:ucCMSMenu ID="ctlCMSMenu" runat="server" />
    </div>
    <!--/wrapper-->
</asp:Content>
