﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AJAXFileUpload.aspx.cs" Inherits="SydneyAviators.Web.AJAXFileUpload" %>

<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls" TagPrefix="cc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body style="margin-left: 0; background-color: Transparent;">
    <form id="formAjaxFileUpload" runat="server">
    <div>
        <div style="float: left;">
            <cc:FileUpload ID="fileUpload" runat="server" />
        </div>
        <div style="float: left;">
            <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" />&nbsp;
            <asp:Label ID="lblUploaded" runat="server" Text="<strong>Uploaded</strong>" />
        </div>
    </div>
    </form>
</body>
</html>
