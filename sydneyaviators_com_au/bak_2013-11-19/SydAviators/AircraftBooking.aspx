﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Main.Master" AutoEventWireup="true"
    CodeBehind="AircraftBooking.aspx.cs" Inherits="SydneyAviators.Web.AircraftBooking" %>

<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls"
    TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ucBreadcrumb.ascx" TagName="ucBreadcrumb" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        //<![CDATA[
        function initialiseGrid() {
            $(".timeHalf").click(function () { onTimeCellClicked(this); });

            if (parseInt(document.getElementById("<%=hidSelectionStartCellNumber.ClientID%>").value) != NaN && document.getElementById("<%=hidSelectedTime.ClientID%>").value != "")
            {
                var rowNumber = parseInt(document.getElementById("<%=hidSelectionStartRowNumber.ClientID%>").value);
                setSelected(rowNumber, getCellNumberByTime($("OPTION:selected", "#<%= tpStartTime.ClientID %>").val()));
            }
            else if (parseInt(document.getElementById("<%=hidSelectionStartCellNumber.ClientID%>").value) != NaN && parseInt(document.getElementById("<%=hidSelectionStartCellNumber.ClientID%>").value) >= 0) {
                var cellNumber = parseInt(document.getElementById("<%=hidSelectionStartCellNumber.ClientID%>").value);
                var rowNumber = parseInt(document.getElementById("<%=hidSelectionStartRowNumber.ClientID%>").value);
                setSelected(rowNumber, cellNumber);
                setSelectedStartTimeByCellNumber(rowNumber, cellNumber);
            }
            else
                setSelected(1, getCellNumberByTime($("OPTION:selected", "#<%= tpStartTime.ClientID %>").val()));
        }

        function onTimeCellClicked(cell) {
            var cellNumber = getCellNumberFromClassName(cell.className);
            var rowNumber = getRowNumberFromClassName(cell.className);
            
            setSelected(rowNumber, cellNumber);
            setSelectedStartTimeByCellNumber(rowNumber, cellNumber);
            $get('ctl00_body_ddlAircrafts')[rowNumber - 1].selected = true;
        }

        function setSelected(rowNumber, cellNumber) {
            var iterations = getSelectedDurationInMinutes() / 30;
            clearSelection();

            if (cellNumber < 0) {
                document.getElementById("<%=hidSelectionStartCellNumber.ClientID%>").value = "";
                document.getElementById("<%=hidSelectionStartRowNumber.ClientID%>").value = "";
                return;
            }
            var message = false;
            for (var i = 0; i < iterations; i++) {
                if (message == true)
                    break;
                message = true;
                $(".row" + rowNumber + ".cell" + (cellNumber + i)).each(function (i) {
                    if (this.className.indexOf("selected") < 0) {
                        message = false;
                        this.className = this.className + " selected";
                    }
                });
            }
            if (message == true) {
                window.alert('Time Unavailable!');
                for (var i = 0; i < iterations; i++) {
                    message = true;
                    $(".row" + rowNumber + ".cell" + (cellNumber + i)).each(function (i) {
                        if (this.className.indexOf("selected") > 0) {
                            this.className = this.className.replace(' selected', '');
                        }
                    });
                }
            }

            document.getElementById("<%=hidSelectionStartCellNumber.ClientID%>").value = cellNumber;
            document.getElementById("<%=hidSelectionStartRowNumber.ClientID%>").value = rowNumber;
        }

        function getCellNumberFromClassName(className) {
            var tmp = className.substr(className.indexOf("cell"), className.length - className.indexOf("cell"));

            if (tmp.indexOf(" ") > 0)
                tmp = tmp.substr(0, tmp.indexOf(" "));

            tmp = tmp.substr(4, tmp.length - 4);

            return parseInt(tmp);
        }

        function getRowNumberFromClassName(className) {
            var tmp = className.substr(className.indexOf("row"), className.length - className.indexOf("cell") - 1);

            if (tmp.indexOf(" ") > 0)
                tmp = tmp.substr(0, tmp.indexOf(" "));

            tmp = tmp.substr(3, tmp.length - 3);

            return parseInt(tmp);
        }

        function getCellNumberSelectionStart() {
            var minCellNumber = 999;
            var cellNumber;

            $(".timeHalf.row1").each(function (i) {
                if (this.className.indexOf(" selected") >= 0) {
                    cellNumber = getCellNumberFromClassName(this.className);
                    if (cellNumber < minCellNumber)
                        minCellNumber = cellNumber;
                }
            });

            return minCellNumber;
        }

        function onDurationChanged() {
            setSelected(getCellNumberSelectionStart());
        }

        function onStartTimeChanged() {
            var startTime = $("OPTION:selected", "#<%= tpStartTime.ClientID %>").val();
            var cellNumber = getCellNumberByTime(startTime);

            if (cellNumber >= 0)
                setSelected(getCellNumberByTime(startTime));
            else
                clearSelection();
        }

        function getCellNumberByTime(time) {
            var cellNumber = -1;

            $(".timeHalf.row1").each(function (i) {
                if ($(this).children("input[type='hidden']").val() == time)
                    cellNumber = getCellNumberFromClassName(this.className);
            });
            
            return cellNumber;
        }

        function getSelectedDurationInMinutes() {
            return document.getElementById("<%=hidSelectedDuration.ClientID%>").value;
        }

        function clearSelection() {
            $(".timeHalf").each(function (i) {
                if (this.className.indexOf(" selected") >= 0)
                    this.className = this.className.replace(" selected", "");
            });
        }

        function setSelectedStartTimeByCellNumber(rowNumber, cellNumber) {
            var tp = $("<%= tpStartTime.ClientID %>");
            
            if (tp != null) {
                var startTime = $(".timeHalf.row" + rowNumber + ".cell" + cellNumber).children("input[type='hidden']").val();
                
                document.getElementById("<%=hidSelectedTime.ClientID%>").value = startTime;
                $("#<%= tpStartTime.ClientID %>").val(startTime);
            }
        }

        function calcRate() {
            var litRate = $get('litRate');
            var ddlDuration = $get('ddlDuration');
            var txtAdditionalDays = $get('txtAdditionalDays');
            var hidRate = $get('hidRate');

            litRate.value = litRate.value * ddlDuration.value;
        }

    </script>
</asp:Content>
<asp:Content ID="banner" ContentPlaceHolderID="banner" runat="server">
    <div class="banner">
        <div class="banner-title">
            <h1>
                <asp:Literal ID="litPageTitle" runat="server" />&nbsp;
                <span><asp:Literal ID="litSEOTitle" runat="server" /></span></h1>
        </div>
        <asp:HyperLink ID="lnkEnquiry" runat="server" CssClass="btn-enquiry">Make an enquiry</asp:HyperLink>
    </div>
    <!--/banner-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hidSelectionStartCellNumber" runat="server" />
            <asp:HiddenField ID="hidSelectionStartRowNumber" runat="server" />
            <asp:HiddenField ID="hidSelectedDuration" runat="server" />
            <asp:HiddenField ID="hidSelectedTime" runat="server" />
            <asp:HiddenField ID="hidRate" runat="server" />
            <asp:HiddenField ID="hidAircraftID" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="wrapper fullwidth">
        <uc:ucBreadcrumb ID="ctlBreadcrumb" runat="server" Visible="false"/>
        <p class="breadcrumbs">
            <a href="./">Home</a>
             <span class="sep">&gt; </span>
             <asp:HyperLink ID="lnkAircraftHire" runat="server"><span>Aircraft Hire</span></asp:HyperLink>
             <span class="sep">&gt; </span>Aircraft Booking
            </p>

        <div class="main">
            <div class="content">
<%--                <ul class="tabs">
                    <li><a class="current" href="">By Hour</a></li>
                    <li><a href="">By Day</a></li>
                </ul>--%>
                <div class="form">
                    <table cellspacing="0" class="form-table form-table-2cols">
                        <colgroup>
                            <col width="15%">
                            <col width="35%">
                            <col width="15%">
                            <col width="35%">
                        </colgroup>
                        <tbody>
                            <tr>
                                <td>
                                    <label>
                                        Aircraft Type</label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlAircraftType" runat="server" DataTextField="FullName" DataValueField="ResourceTypeID"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlAircraftType_OnSelectedIndexChanged" />
                                </td>
                                <td>
                                    <label>
                                        Booking Date</label>
                                </td>
                                <td>
                                    <cc1:DatePicker ID="dpBookingDate" runat="server" DateTextBoxWidth="132px"  AutoPostBack="true"
                                        OnSelectedDateChanged="dpBookingDate_SelectedDateChanged" CalendarIconVisible="False" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        Aircraft</label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlAircrafts" runat="server" DataTextField="FullName" DataValueField="ResourceID"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlAircrafts_OnSelectedIndexChanged" />
                                </td>
                                <td>
                                    <label>
                                        Start Time</label>
                                </td>
                                <td>
                                    <cc1:TimePicker ID="tpStartTime" runat="server" Width="142px" RangeFilter="08:00-17:00" AutoPostBack="true"
                                        OnSelectedIndexChanged="dpStartTime_SelectedIndexChanged" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        Type</label>
                                </td>
                                <td>
                                    <div class="inline">
                                        <asp:RadioButton ID="rdbSolo" runat="server" Text="Solo" GroupName="BookType" Checked="true"
                                            AutoPostBack="true" OnCheckedChanged="rdbSolo_OnCheckedChanged" /><asp:RadioButton
                                                ID="rdbDual" runat="server" Text="Dual" AutoPostBack="true" GroupName="BookType"
                                                OnCheckedChanged="rdbDual_OnCheckedChanged" />
                                    </div>
                                </td>
                                <td>
                                    <label>
                                        Booking Duration</label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDuration" Width="142px" runat="server" AutoPostBack="true" 
                                        OnSelectedIndexChanged="ddlDuration_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        Rate</label>
                                </td>
                                <td class="rate">
                                    <asp:Literal ID="litRate" runat="server" />
                                </td>
                                <td>
                                    <label>
                                        Time Format</label>
                                </td>
                                <td>
                                    <div class="time-btns">
                                        <asp:LinkButton ID="lnk12HourFormat" runat="server" CssClass="timeBTN-L active" OnClick="lnk12HourFormat_OnClick">
                                        12 hr</asp:LinkButton>
                                        <asp:LinkButton ID="lnk24HourFormat" runat="server" CssClass="timeBTN-R" OnClick="lnk24HourFormat_lnk24HourFormat">24 hr</asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:LinkButton ID="lkbLogin" Text="Login" runat="server" OnClick="lkbLogin_OnClick" Visible="false" />
                </div>
                <div id="divInstructor" runat="server" class="form">
                    <table class="form-table" cellspacing="0">
                        <tr>
                            <td>
                                <label>
                                    Instructor</label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlInstructor" runat="server" DataTextField="FullName" DataValueField="ContactID"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlInstructor_OnSelectedIndexChanged" />
                                <asp:Literal ID="litInstructor" runat="server" />
                                <asp:HiddenField ID="hidInstructor" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>           
            <!--/form-->
            <div class="timeChart">
              <asp:LinkButton runat="server" ID="lnkBookTop" class="btn right" Text="<span>Book Now</span>"
                    OnClick="btnBook_OnClick"></asp:LinkButton><br /><br />
                <div class="timeChart-nav group">
                    <asp:LinkButton ID="btnPrevious6HoursTop" runat="server" Text="<span>Prev 6 Hours</span>"
                        ToolTip="Prev 6 Hours" OnClick="btnPrevious6Hours_Click" CssClass="prev" Visible="false" />
                         <div class="key-wrap">
                        <span class="label">Key</span>
                        <div class="key">
                            <ul>
                                <li>Available
                                    <img src="App_Themes/SydneyAviators/images/key-available.png" alt="Available" /></li>
                                <li>Not Available
                                    <img src="App_Themes/SydneyAviators/images/key-not-available.png" alt="Not Available" /></li>
                                <li>Selected
                                    <img src="App_Themes/SydneyAviators/images/key-selected.png" alt="Selected" /></li>
                            </ul>
                        </div>
                    </div>
                    <asp:LinkButton ID="btnNext6HoursTop" runat="server" Text="<span>Next 6 Hours</span>"
                        ToolTip="Next 6 Hours" OnClick="btnNext6Hours_Click" CssClass="next" Visible="false" />                   
                </div>
                <table width="914" border="0" cellspacing="0" cellpadding="0" class="timeChart-table">
                    <asp:Repeater ID="rptAircrafts" runat="server" OnItemDataBound="rptAircrafts_ItemDataBound">
                        <ItemTemplate>
                            <tr>
                                <td id="tdCity" runat="server" class="name">
                                    <h2>
                                        <asp:Literal ID="litAircraft" runat="server" /></h2>
                                    <p>
                                        <asp:Literal ID="litReference" runat="server" /></p>
                                    <p id="pHostType" runat="server" class="hostType">
                                        <asp:Literal ID="litAircraftType" runat="server" /></p>
                                </td>
                                <asp:Repeater ID="rptTimeBlock" runat="server">
                                    <ItemTemplate>
                                        <td id="tdTimeBlock" runat="server" class="blue">
                                            <div class="timeHolder">
                                                <div id="divTimeHalf2" runat="server" class="timeHalf b">
                                                    <asp:HiddenField ID="hidTime2" runat="server" />
                                                </div>
                                                <div id="divTimeHalf1" runat="server" class="timeHalf">
                                                    <asp:HiddenField ID="hidTime1" runat="server" />
                                                    <p class="time">
                                                        <asp:Literal ID="litTime" runat="server" />
                                                        <span>
                                                            <asp:Literal ID="litAMPM" runat="server" /></span></p>
                                                </div>
                                            </div>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
                <div class="timeChart-nav group">
                    <asp:LinkButton ID="btnPrevious6Hours" runat="server" Text="<span>Prev 6 Hours</span>"
                        ToolTip="Prev 6 Hours" OnClick="btnPrevious6Hours_Click" CssClass="prev" Visible="false" />
                    <div class="key-wrap">
                        <span class="label">Key</span>
                        <div class="key">
                            <ul>
                                <li>Available
                                    <img src="App_Themes/SydneyAviators/images/key-available.png" alt="Available" /></li>
                                <li>Not Available
                                    <img src="App_Themes/SydneyAviators/images/key-not-available.png" alt="Not Available" /></li>
                                <li>Selected
                                    <img src="App_Themes/SydneyAviators/images/key-selected.png" alt="Selected" /></li>
                            </ul>
                        </div>
                    </div>
                    <asp:LinkButton ID="btnNext6Hours" runat="server" Text="<span>Next 6 Hours</span>"
                        ToolTip="Next 6 Hours" OnClick="btnNext6Hours_Click" CssClass="next" Visible="false" />
                </div>
                <asp:Button runat="server" ID="btnBook" class="btn right" Text="Book Now" OnClick="btnBook_OnClick"
                    Visible="false" />
                <asp:LinkButton runat="server" ID="lnkBook" class="btn right" Text="<span>Book Now</span>"
                    OnClick="btnBook_OnClick"></asp:LinkButton>
            </div>
            <!--/timeChart-->
        </div>
         <!--/main-->
       
    </div>
    <!--/wrapper full width-->
     </div>
     <!--/content-->
    <!--/wrapper-->
    <!--/wrapper-->
</asp:Content>
