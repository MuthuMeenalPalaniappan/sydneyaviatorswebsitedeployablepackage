﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Main.Master" AutoEventWireup="true"
    CodeBehind="Aircraft.aspx.cs" Inherits="SydneyAviators.Web.Aircraft" %>

<%@ Register Src="~/Controls/ucBreadcrumb.ascx" TagName="ucBreadcrumb" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucCMSMenu.ascx" TagName="ucCMSMenu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucCMSAttachment.ascx" TagName="ucCMSAttachment" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ucFacebookComments.ascx" TagName="ucFacebookComments" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="banner" runat="server">
    <div class="banner">
        <div class="banner-title">
            <h1>
                <asp:Literal ID="litPageTitle" runat="server" Text="Aircraft Hire" /><span>&nbsp;
                    <asp:Literal ID="litSEOTitle" runat="server" /></span></h1>
        </div>
        <asp:HyperLink ID="lnkEnquiry" runat="server" CssClass="btn-enquiry">Make an enquiry</asp:HyperLink>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="wrapper">
        <uc:ucBreadcrumb ID="ctlBreadcrumb" runat="server" />
        <div class="main">
            <div class="content">
                <h2>
                    Aircraft Hire</h2>
                <div class="detail detail-sup">
                    <div class="item">
                        <div class="img">
                            <asp:Image ID="imgThumbnail" runat="server" CssClass="thumb" Width="140px" Height="140px" /></div>
                        <div class="copy">
                            <h4>
                                <asp:Literal ID="litTitle" runat="server" /></h4>
                            <asp:Literal ID="litContent" runat="server" />
                             <uc:ucFacebookComments ID="ctlFacebookComments" runat="server" />
                        </div>
                        <div class="sub-copy">
                            <div class="item">
                                <div class="txt">
                                    <p class="rate">
                                        <span class="sml-title">Solo</span> <asp:HyperLink ID="lnkSoloRates" runat="server">
                                            <asp:Literal ID="litSoloRates" runat="server" /></asp:HyperLink> Per hour</p>
                                    <p class="rate">
                                        <span class="sml-title">Dual</span> <asp:HyperLink ID="lnkDualRates" runat="server">
                                            <asp:Literal ID="litDualRates" runat="server" /></asp:HyperLink> Per hour</p>
                                    <p class="rate">
                                        <span class="sml-title">Landing Fees</span> <asp:HyperLink ID="lnkLandingFees" runat="server">
                                            <asp:Literal ID="litLandingFees" runat="server" /></asp:HyperLink></p>
                                    <p>
                                        Additional fees and services may apply.</p>
                                </div>
                                <asp:HyperLink ID="lnkBookNow" runat="server" CssClass="btn"><span>Book Now</span></asp:HyperLink>
                            </div>
                        </div>
                    </div>
                    <!--/item-->
                </div>
                <!--/listing-detail-->
            </div>
            <!--/content-->
            <div class="sup-content">
            <uc:ucCMSAttachment ID="ctlCMSAttachment" runat="server" />
              </div>
            <!--/sup-content-->
        </div>
        <!--/main-->
        <uc:ucCMSMenu ID="ctlCMSMenu" runat="server" />
    </div>
    <!--/wrapper-->
</asp:Content>
