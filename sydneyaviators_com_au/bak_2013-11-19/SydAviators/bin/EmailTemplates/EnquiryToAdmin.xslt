﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text"/>
  <xsl:template match="Data">
    <xsl:text>
Dear Sydney Aviators staff,

A customer have submitted an enquiry with the following details.

First Name:   </xsl:text><xsl:value-of select="Data/FirstName" /><xsl:text>
Last Name:    </xsl:text><xsl:value-of select="Data/LastName" /><xsl:text>
Email:        </xsl:text><xsl:value-of select="Data/Email" /><xsl:text>
Phone:        </xsl:text><xsl:value-of select="Data/Phone" /><xsl:text>
Enquiry Type: </xsl:text><xsl:value-of select="Data/EnquiryType" /><xsl:text>
About:        </xsl:text><xsl:value-of select="Data/About" /><xsl:text>
Comments:     
</xsl:text><xsl:value-of select="Data/Comments" /><xsl:text>

Submitted On: </xsl:text><xsl:value-of select="Data/CreateTS" /><xsl:text>

Regards,
Sydney Aviators Administrator

    </xsl:text>
  </xsl:template>
</xsl:stylesheet>
