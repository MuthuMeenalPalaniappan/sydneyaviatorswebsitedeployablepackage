﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Main.Master" AutoEventWireup="true"
    CodeBehind="ConfirmBooking.aspx.cs" Inherits="SydneyAviators.Web.ConfirmBooking" %>

<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls"
    TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ucBreadcrumb.ascx" TagName="ucBreadcrumb" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="banner" ContentPlaceHolderID="banner" runat="server">
    <div class="banner">
        <div class="banner-title">
            <h1>
                <asp:Literal ID="litPageTitle" runat="server" />&nbsp;
                <span><asp:Literal ID="litSEOTitle" runat="server" /></span></h1>
        </div>
        <asp:HyperLink ID="lnkEnquiry" runat="server" CssClass="btn-enquiry">Make an enquiry</asp:HyperLink>
    </div>
    <!--/banner-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="wrapper fullwidth">
        <uc:ucBreadcrumb ID="ctlBreadcrumb" runat="server" Visible="false"/>
        <p class="breadcrumbs">
            <a href="./">Home</a>
             <span class="sep">&gt; </span>
             <asp:HyperLink ID="lnkAircraftHire" runat="server"><span>Aircraft Hire</span></asp:HyperLink>
             <span class="sep">&gt; </span>Aircraft Booking
        </p>
        <div class="main">
            <div class="content">
                <div class="form">
                     <h4>Your Booking Info</h4>
                      <div id="ctl00_body_valSummary" style="color:Red;display:none;"> </div>
                        <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode ="BulletList" EnableClientScript="true" />
                       <asp:Literal ID="litError" runat="server" Visible = "false"></asp:Literal>             
                    <table class="form-table form-table-2cols" cellspacing="0">
                        <colgroup>
                                <col width="19%" />
                                <col width="31%" />
                                <col width="19%" />
                                <col width="31%" />
                            </colgroup>
                             <tr>
                                <td><label>Aircraft</label></td>
                                <td class="txt-col"> <asp:Literal ID="litResourceTitle" runat="server" /></td>
                                <td><label>Booking Date</label></td>
                                <td class="txt-col"><asp:Literal ID="litScheduleStartDate" runat="server" /></td>
                            </tr>
                            <tr>
                                <td><label>Type</label></td>
                                <td class="txt-col"><asp:Literal ID="litBookingType" runat="server" /></td>
                                <td><label>Start Time</label></td>
                                <td class="txt-col"><asp:Literal ID="litScheduleStartTime" runat="server" /></td>
                            </tr>
                             <tr>
                                <td><label>Rate</label></td>

                                <td class="txt-col"><asp:Literal ID="litRate" runat="server" /> / h</td>
                                <td><label>Booking Duration</label></td>
                                <td class="txt-col"><asp:Literal ID="litScheduleDuration" runat="server" /></td>
                            </tr>
                             <tr>
                                <td><label>Total</label></td>
                                <td class="txt-col"><asp:Literal ID="litTotal1" runat="server" /></td>

                                <td>&nbsp;</td>
                                <td class="txt-col">&nbsp;</td>
                            </tr>                    
                    </table>
                </div>
                <div class="form">
                     <h4> Your Details</h4>

                     <table class="form-table form-table-2cols" cellspacing="0">
                            <colgroup>
                                <col width="19%" />
                                <col width="31%" />
                                <col width="19%" />
                                <col width="31%" />
                            </colgroup>
                             <tr id="rowBookOnAccount" runat="server" visible="false">
                                    <td colspan = "1">
                                        <label>
                                            Book on account
                                        </label>
                                    </td>
                                    <td colspan = "3">
                                        <%-- <asp:CheckBox ID="chkBookOnAccount" runat="server" AutoPostBack="true" OnCheckedChanged="chkBookOnAccount_OnCheckedChanged"/> --%>
                                        <asp:CheckBox ID="chkBookOnAccount" runat="server" AutoPostBack="true" />
                                    </td>
                                </tr>
                            <tr>
                                <td><label>First Name</label></td>

                                <td> <asp:TextBox ID="txtFirstName" runat="server" TabIndex="1" CssClass="text"/></td>
                                <%-- 
                                <td><asp:Label ID="lblCardHolderName" runat="server" Text="Credit Card Holder Name *"></asp:Label></td>
                                <td>
                                 <asp:TextBox ID="txtCardHolderName" runat="server" TabIndex="4" CssClass="text"/>
                                 <asp:RequiredFieldValidator ID="valCardHolderName" runat="server" ErrorMessage="Invalid Card Holder Name<br>" ControlToValidate="txtCardHolderName">*</asp:RequiredFieldValidator>                             
                                  
                                </td>
                                --%>
                            </tr>
                            <tr>
                                <td><label>Last Name</label></td>
                                <td><asp:TextBox ID="txtLastName" runat="server" TabIndex="2" CssClass="text"/></td>
                                <%-- <td><asp:Label ID="lblCreditCardNumber" runat="server" Text="Credit Card Number *"></asp:Label></td>
                                <td>
                                <asp:TextBox ID="txtCardNumber" runat="server" TabIndex="5" CssClass="text"/>
                                <asp:RequiredFieldValidator ID="valCardNumber" runat="server" ErrorMessage="Invalid Card Number<br>" ControlToValidate="txtCardNumber">*</asp:RequiredFieldValidator>
                                </td>
                                --%>
                            </tr>
                            <tr>
                                <td><label>Email</label></td>
                                <td> <asp:TextBox ID="txtEmail" runat="server"  TabIndex="3" CssClass="text"  jVal="{valid:/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/, message:'Invalid Email Address'}" jValKey="{valid:/[a-zA-Z0-9._%+-@]/, cFunc:'alert', cArgs:['Email Address: '+$(this).val()]}" /></td>
                                <%--<td><asp:Label ID="lblCreditCardExpiry" runat="server" Text="Credit Card Expiry *"></asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="ddlCreditCardExpiryMonth" runat="server" Width="60px" TabIndex="6"/>
                                    <asp:RequiredFieldValidator ID="valCardExpiryMonth" runat="server" ErrorMessage="Invalid Card Expiry Month<br>" ControlToValidate="ddlCreditCardExpiryMonth">*</asp:RequiredFieldValidator>
                                        &nbsp; <asp:Label ID="lblDateBackSlash" runat="server" Text="/"></asp:Label>
                                    <asp:DropDownList ID="ddlCreditCardExpiryYear" runat="server" Width="70px" TabIndex="7"/>
                                    <asp:RequiredFieldValidator ID="valCardExpiryYear" runat="server" ErrorMessage="Invalid Card Expiry Year<br>" ControlToValidate="ddlCreditCardExpiryYear">*</asp:RequiredFieldValidator>
                                   
                                </td>--%>
                            </tr>


                       
                    </table>
                    <asp:LinkButton ID="lkbLogin" Text="Login" runat="server" OnClick="lkbLogin_OnClick" CausesValidation = "false" Visible="false"/>
                </div>              
               
                <!--/form-->
                  <div class="form">
                    
                    	<p class="total-rate">Total <strong><asp:Literal ID="litTotal2" runat="server" /></strong></p>
                        
                    </div>
                
                 <div id="ctl00_body_divTotal" class="form">
                    	
                        <div class="right">
                            <div class="left" style="padding: 4px 32px 0 0;">
                        	    <asp:CheckBox ID="chkAgreeTAndC" runat="server" />
                            </div>
                            <!--<input type="submit" name="ctl00$body$btnBook" value="Confirm" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$body$btnBook&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" id="ctl00_body_btnBook" />-->                                                      
                            <asp:LinkButton runat="server" ID="lnkConfirm" class="btn right" Text="<span>Confirm</span>" OnClick="btnBook_OnClick"></asp:LinkButton>                                   
                            <asp:LinkButton runat="server" ID="lnkBack" class="btn3 right" Text="<span>Back</span>" OnClick="btnBack_OnClick" CausesValidation = "false"></asp:LinkButton>   
                        </div>                        
                    </div>               
            </div>
            <!--/main-->
        </div>
    </div>
    <!--/wrapper-->
</asp:Content>
