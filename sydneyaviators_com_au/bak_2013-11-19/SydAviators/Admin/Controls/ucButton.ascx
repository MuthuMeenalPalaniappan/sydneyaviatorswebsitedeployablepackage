﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucButton.ascx.cs" Inherits="SydneyAviators.Web.Admin.Controls.ucButton" %>

<asp:LinkButton ID="btn" runat="server" CssClass="btn" OnClick="btn_Click">
    <span>
        <asp:Literal ID="litText" runat="server" /></span></asp:LinkButton>