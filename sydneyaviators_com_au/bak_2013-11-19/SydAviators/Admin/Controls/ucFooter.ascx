﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucFooter.ascx.cs" Inherits="SydneyAviators.Web.Admin.Controls.ucFooter" %>


<div id="footer">
        <div class="container">
            <div id="footer-sidebar">
                <p>&copy; Copyright 2010. All Rights Reserved.</p>
            </div>
            <div id="footer-content">
                <p>Sydney Aviators</p>

            </div>
        </div> 
      <!-- /.container -->
    </div> <!-- /#footer -->