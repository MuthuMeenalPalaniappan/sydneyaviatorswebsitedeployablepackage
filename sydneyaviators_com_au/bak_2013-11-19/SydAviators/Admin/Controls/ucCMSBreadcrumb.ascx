﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCMSBreadcrumb.ascx.cs" Inherits="SydneyAviators.Web.Admin.Controls.ucCMSBreadcrumb" %>
<asp:PlaceHolder ID="phContent" runat="server">
    <p id="breadcrumbs">
        <asp:Repeater ID="rptBreadcrumb" runat="server" OnItemDataBound="rptBreadcrumb_ItemDataBound">
            <ItemTemplate>
                <asp:HyperLink ID="lnkMenu" runat="server" />
                <asp:PlaceHolder ID="phArrow" runat="server"><span>
                    <asp:Image ID="imgArrowBreadcrumb" runat="server" SkinID="imgArrowBreadcrumb" />&nbsp;</span>
                </asp:PlaceHolder>
            </ItemTemplate>
        </asp:Repeater>
    </p>
</asp:PlaceHolder>
