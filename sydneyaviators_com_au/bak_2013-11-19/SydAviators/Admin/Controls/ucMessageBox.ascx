﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucMessageBox.ascx.cs" Inherits="SydneyAviators.Web.Admin.Controls.ucMessageBox" %>
<asp:Panel ID="pnlMessage" runat="server" CssClass="information">
    <strong><asp:Literal ID="litMessageTitle" runat="server"></asp:Literal></strong><asp:Literal ID="litMessageDetail" runat="server"></asp:Literal>
</asp:Panel>		
