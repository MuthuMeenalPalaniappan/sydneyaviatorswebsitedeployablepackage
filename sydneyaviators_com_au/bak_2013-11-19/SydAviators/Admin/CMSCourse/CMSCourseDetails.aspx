﻿<%@ Page Language="C#" MasterPageFile="~/Admin/MasterPage/Admin.Master" AutoEventWireup="True" CodeBehind="CMSCourseDetails.aspx.cs" Inherits="SydneyAviators.Web.Admin.CMSCourse.CMSCourseDetails" %>

<%@ Register Src="Controls/ucCMSCourseInfo.ascx" TagName="ucInfo" TagPrefix="uc" %>
<%@ Register src="~/Admin/Controls/ucTitleBar.ascx" tagname="ucTitleBar" tagprefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucCMSMenu.ascx" TagName="ucCMSMenu" TagPrefix="uc" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="sidebar" ContentPlaceHolderID="sidebar" runat="server">
<uc:ucCMSMenu ID="ctlCMSMenu" runat="server" />
</asp:Content>
<asp:Content ID="main" ContentPlaceHolderID="main" runat="server">

<asp:ScriptManager ID="ScriptManagerMain" runat="server" />
    <uc:ucTitleBar id="ctlTitle" runat="server" TitleText="Course Details" />
    <uc:ucInfo ID="ctlInfo" runat="server" />
</asp:Content> 
