﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucCMSPageAdd.ascx.cs"
    Inherits="SydneyAviators.Web.Admin.CMSPage.Controls.ucCMSPageAdd" %>
<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls"
    TagPrefix="cc" %>
<%@ Register Src="~/Admin/Controls/ucModuleHeader.ascx" TagName="ucModuleHeader" TagPrefix="uc" %>
<%@ Register Src="~/Admin/Controls/ucMessageBox.ascx" TagName="ucMessage" TagPrefix="uc" %>
<div class="module">
    <uc:ucModuleHeader ID="moduleHeaderFilter" ControlToToggle="pnlAdd" LoadCollapsed="true"
        TitleText="Add Pages" runat="server" />
    <asp:Panel ID="pnlAdd" runat="server" CssClass="pane">
        <p>
            Select the type of page you want to add.</p>
        <div class="wrap">
            <table class="form-table" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="20%">
                        <label>
                            Template</label>
                    </td>
                    <td width="30%">
                        <asp:DropDownList ID="ddlTemplate" runat="server" CssClass="select span-4" />
                    </td>
                    <td width="20%">
                        <label>
                        </label>
                    </td>
                    <td width="30%">
                    </td>
                </tr>
            </table>
        </div>
        <div class="actions">
            <div class="left">
                <asp:LinkButton ID="btnAdd" CssClass="btn" runat="server" OnClick="btnAdd_Click"><span>Add</span></asp:LinkButton>
            </div>
            <div class="right">
            </div>
        </div>
        <uc:ucMessage ID="ctlMessage" runat="server" />
        
    </asp:Panel>
</div>
