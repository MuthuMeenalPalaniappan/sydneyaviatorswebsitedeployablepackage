﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucCMSHomepageInfo.ascx.cs" Inherits="SydneyAviators.Web.Admin.CMSHomepage.Controls.ucCMSHomepageInfo" %>
<%@ Register Assembly="SydneyAviators.WebControls" Namespace="SydneyAviators.WebControls" TagPrefix="cc" %>
<%@ Register Src="ucCMSHomepageDetails.ascx" TagName="ucDetails" TagPrefix="uc" %>
<%@ Register Src="ucCMSHomepageSliderSearchList.ascx" TagName="ucSliderImages" TagPrefix="uc" %>
<%@ Register Src="~/Admin/CMSPage/Controls/ucCMSPageDetails.ascx" TagName="ucPageDetails" TagPrefix="uc" %>

<div class="form-wrap">
    <asp:UpdatePanel ID="upTabs" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <ul class="tabs">
                <li>
                    <asp:LinkButton ID="lnkDetails" runat="server" CssClass="current" OnClick="lnkDetails_Click">Homepage Details</asp:LinkButton></li>
                <li>
                    <asp:LinkButton ID="lnkPageDetails" runat="server" OnClick="lnkPageDetails_Click">SEO Details</asp:LinkButton></li>
            </ul>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="pane">
        <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlDetails" runat="server" Visible="true">
                    <uc:ucDetails ID="ctlDetails" runat="server" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upPageDetails" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlPageDetails" runat="server" Visible="false">
                    <uc:ucPageDetails ID="ctlPageDetails" runat="server" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <br />
    <br />
    <asp:Panel ID="pnBottom" runat="server">
        <asp:UpdatePanel ID="upBottomTabs" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="tabs">
                    <li>
                        <asp:LinkButton ID="lnkSliderImages" runat="server" CssClass="current" OnClick="lnkSliderImages_Click">Promo Images</asp:LinkButton></li>
                </ul>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="pane">
            <asp:UpdatePanel ID="upSliderImages" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlSliderImages" runat="server" Visible="true">
                        <uc:ucSliderImages ID="ctlSliderImages" runat="server" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
</div>
