﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCMSAttachmentInfo.ascx.cs"
    Inherits="SydneyAviators.Web.Admin.CMSAttachment.Controls.ucCMSAttachmentInfo" %>
<%@ Register Src="~/Admin/CMSAttachment/Controls/ucCMSAttachmentSearchList.ascx"
    TagName="ucAttachmentList" TagPrefix="uc" %>
<%@ Register Src="~/Admin/CMSAttachment/Controls/ucCMSAttachmentDetails.ascx" TagName="ucAttachmentDetails"
    TagPrefix="uc" %>
<asp:UpdatePanel ID="upAttachmentInfo" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <uc:ucAttachmentDetails ID="ctlAttachmentDetails" runat="server" Visible="false"
            OnSaved="ctlAttachmentDetails_Saved" OnCancelled="ctlAttachmentDetails_Cancelled" />
        <uc:ucAttachmentList ID="ctlAttachmentList" runat="server" OnAdd="ctlAttachmentList_Add"
            OnItemSelected="ctlAttachmentList_ItemSelected" />
    </ContentTemplate>
</asp:UpdatePanel>
