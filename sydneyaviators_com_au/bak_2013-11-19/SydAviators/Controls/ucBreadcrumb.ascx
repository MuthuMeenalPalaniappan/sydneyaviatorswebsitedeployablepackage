﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucBreadcrumb.ascx.cs"
    Inherits="SydneyAviators.Web.Controls.ucBreadcrumb" %>
<p class="breadcrumbs">

<asp:HyperLink ID="lnkHome" runat="server" Text="Home" />
<asp:PlaceHolder ID="phContactUs" runat="server" Visible="false">
<span class="sep">&gt; </span>Contact Us
</asp:PlaceHolder>
<asp:Repeater ID="rptBreadcrumb" runat="server" OnItemDataBound="rptBreadcrumb_ItemDataBound">
<ItemTemplate> <span class="sep">&gt; </span><asp:HyperLink ID="lnkMenu" runat="server" Visible="false" /><asp:Literal ID="litMenu" runat="server" /></ItemTemplate>
</asp:Repeater>
</p>
