﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCMSHomepageFacebook.ascx.cs"
    Inherits="SydneyAviators.Web.Controls.ucCMSHomepageFacebook" %>
<%@ Register Src="~/Controls/ucFooter.ascx" TagName="ucFooter" TagPrefix="uc" %>
<div class="face">
    <div id="facebook-container">
        <div class="facebook-wrapper">
            <div class="facebook-tile">
                <h2>
                    Follow us on Facebook</h2>
            </div>
            <div id="facebook">
                <div id="likebox-frame">
                    <iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FSYDNEY-AVIATORS%2F115353418511583&amp;width=887&amp;colorscheme=light&amp;show_faces=false&amp;stream=true&amp;header=false&amp;height=395"
                        scrolling="no" frameborder="0" style="border: none; overflow: hidden; width: 887px;
                        height: 395px;" allowtransparency="true"></iframe>
                </div>
            </div>
        </div>
       <uc:ucFooter ID="ctlFooter" runat="server" />
    </div>
</div>

