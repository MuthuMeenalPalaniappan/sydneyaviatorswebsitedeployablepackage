﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucFooter.ascx.cs" Inherits="SydneyAviators.Web.Controls.ucFooter" %>
<div id="footer">
    <p class="left">
        &copy; 2010 Sydney Aviators Pty Ltd.
        <asp:HyperLink ID="lnkTerms" runat="server">Privacy Policy</asp:HyperLink>
        <br />
        Website by <a href="http://www.levo.com.au" title="www.levo.com.au">Levo</a> &copy;
        2010
    </p>
    <p class="right">
        Follow Us
        <asp:HyperLink ID="lnkFacebook" runat="server" ToolTip="Facebook" Target="_blank"
            NavigateUrl="http://www.facebook.com/pages/Sydney-Australia/SYDNEY-AVIATORS/115353418511583">
            <asp:Image ID="imgFacebook" runat="server" SkinID="imgFacebook" /></asp:HyperLink>&nbsp;<asp:HyperLink
                ID="lnkTwitter" runat="server" ToolTip="Twitter" Target="_blank" NavigateUrl="http://twitter.com/SydneyAviators">
                <asp:Image ID="imgTwitter" runat="server" SkinID="imgTwitter" /></asp:HyperLink></p>
</div>
<!--/footer-->