﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/Main.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="SydneyAviators.Web.Default" %>

<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="banner" runat="server" ContentPlaceHolderID="banner">
    <div class="home-banner">
        <div id="browsable" class="scrollable">
            <asp:Repeater ID="rptImages" runat="server" OnItemDataBound="rptImages_ItemDataBound">
                <HeaderTemplate>
                    <div class="items">
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Panel ID="pnlItem" runat="server" CssClass="item">
                        <div class="banner-title">
                            <h1>
                                <asp:Literal ID="litTitle" runat="server" /><span><asp:Literal ID="litSubTitle" runat="server" /></span></h1>
                            <asp:Literal ID="litSummary" runat="server" />
                        </div>
                    </asp:Panel>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                    <!--/items-->
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <!--/scrollable-->
        <!-- Scroller Navigation -->
        <div class="navi">
        </div>
        <div class="promo-wrap">
            <div class="promo-item promo-1">
                <div class="inner">
                    <h4>
                        Learn to Fly</h4>
                    <p>
                        Would you like to fly with passionate experts?</p>
                    <asp:HyperLink ID="lnkCourses" runat="server" CssClass="btn2"><span>Find out more</span></asp:HyperLink>
                </div>
            </div>
            <div class="promo-item promo-2">
                <div class="inner">
                    <h4>
                        Aircraft Hire</h4>
                    <p>
                        Exceptional range of aircraft available!</p>
                    <asp:HyperLink ID="lnkAircrafts" runat="server" CssClass="btn2"><span>See our aircraft</span></asp:HyperLink>
                </div>
            </div>
        </div>
        <!--/promo-wrap-->
    </div>
    <!--/home-banner-->
</asp:Content>
<asp:Content ID="body" runat="server" ContentPlaceHolderID="body">
    <div class="wrapper cols3-home">
        <div class="main">
            <div class="col1">
                <h2>
                    News &amp; Events</h2>
                <div class="listing">
                    <asp:Repeater ID="rptEvents" runat="server" OnItemDataBound="rptEvents_ItemDataBound">
                        <ItemTemplate>
                            <div class="item">
                                <div class="img">
                                    <asp:HyperLink ID="lnkThumbnail" runat="server">
                                        <asp:Image ID="imgThumbnail" runat="server" CssClass="thumb" />
                                    </asp:HyperLink></div>
                                <div class="copy">
                                    <span class="sml-title">
                                        <asp:Literal ID="litDate" runat="server" /></span>
                                    <h4>
                                        <asp:HyperLink ID="lnkTitle" runat="server" /></h4>
                                    <p>
                                        <asp:Literal ID="litSummary" runat="server" />...<asp:HyperLink ID="lnkMore" runat="server"
                                            CssClass="more" Text="more" /></p>
                                </div>
                            </div>
                            <!--/item-->
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <!--/listing-->
                <asp:HyperLink ID="lnkEvents" runat="server" CssClass="btn-text"><span>View all Updates</span></asp:HyperLink>
            </div>
            <!--/col1-->
            <div class="col2">
                <h2>
                    About Sydney Aviators</h2>
                <p>
                    <asp:Image ID="imgAbout" runat="server" CssClass="thumb" ImageUrl="~/Uploads/home_About.jpg" /></p>
                <p class="intro">
                    Sydney Aviators specialise in teaching to self-paced aviators, training is organised
                    around the student's needs.</p>
                <p>
                    We are conveniently located in the heart of Bankstown Airport at the end of Airport
                    Avenue overlooking the runway. Current licence holders are welcome to rent our quality
                    aircraft from our fleet and use the premises for flight planning. Sydney Aviators
                    has created an atmosphere of an Aeroclub without the cost, you can come and train
                    or hire in a fun, welcoming and social environment.</p>
                <asp:HyperLink ID="lnkAboutUs" runat="server" CssClass="btn-text"><span>Read more</span></asp:HyperLink>
            </div>
            <!--/col2-->
            <div class="col3">
                <h2>
                    Popular Links</h2>
                <h4>
                    Learn to Fly</h4>
                <ul class="links">
                    <li><a href="<%= ResolveUrl(SydneyAviators.Web.Utilities.Navigation.Web.Course(11)) %>">
                        Private Pilot Licence PPL</a></li>
                    <li><a href="<%= ResolveUrl(SydneyAviators.Web.Utilities.Navigation.Web.Course(36)) %>">
                        Commercial Pilot Licence Certificate CPL</a></li>
                </ul>
                <h4>
                    Trial Introductory Flight</h4>
                <ul class="links">
                    <li><a href="<%= ResolveUrl(SydneyAviators.Web.Utilities.Navigation.Web.Course(SydneyAviators.Web.Utilities.Cache.PageID_IntroductoryFlight)) %>">
                        First Solo Flying Camp</a></li>
                </ul>
                <h4>
                    Events</h4>
                <ul class="links">
                    <li><a href="<%= ResolveUrl(SydneyAviators.Web.Utilities.Navigation.Web.EventListing(SydneyAviators.Web.Utilities.Cache.PageID_Events)) %>">
                        Williamtown Defence Force Airshow</a></li>
                </ul>
                <asp:HyperLink ID="lnkAllServices" runat="server" CssClass="btn-text"><span>See all Services</span></asp:HyperLink>
            </div>
            <!--/col3-->
        </div>
        <!--/main-->
    </div>
</asp:Content>
