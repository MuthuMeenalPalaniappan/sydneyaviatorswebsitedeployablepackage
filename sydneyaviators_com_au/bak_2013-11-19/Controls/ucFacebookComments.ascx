﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucFacebookComments.ascx.cs"
    Inherits="SydneyAviators.Web.Controls.ucFacebookComments" %>
<!--facebook-->
<div class="facebook-comments-wrapper">
    <div id="inside_comments">
        <h2><asp:Literal ID="litCompanyName" runat="server" /> Say</h2>
        <div id="fb-root" style="background: #ceeeff;">
        </div>
        <script src="http://connect.facebook.net/en_US/all.js#appId=APP_ID&amp;xfbml=1"></script>
        <fb:comments href='<%= this.URL %>' num_posts="2"
            width="450"></fb:comments>
    </div>
</div>
<!--/facebook-->
