﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCMSAttachment.ascx.cs"
    Inherits="SydneyAviators.Web.Controls.ucCMSAttachment" %>
<asp:Repeater ID="rptItems" runat="server" OnItemDataBound="rptItems_ItemDataBound">
    <ItemTemplate>
        <div class="item">
            <h4>
                <asp:Literal ID="litTitle" runat="server" /></h4>
            <asp:Literal ID="litDescription" runat="server" />
            <asp:HyperLink ID="lnkDownload" runat="server" CssClass="btn-text" Target="_blank"><span>Download File</span></asp:HyperLink>
        </div>
        <!--/item-->
    </ItemTemplate>
</asp:Repeater>
