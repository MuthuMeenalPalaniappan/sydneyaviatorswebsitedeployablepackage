﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCMSMenu.ascx.cs" Inherits="SydneyAviators.Web.Controls.ucCMSMenu" %>
<div class="sidebar">
    <asp:Repeater ID="rptMenu" runat="server" OnItemDataBound="rptMenu_ItemDataBound">
        <HeaderTemplate>
            <ul class="menu">
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <asp:HyperLink ID="lnkMenu" runat="server">
                    <span>
                        <asp:Literal ID="litMenu" runat="server" /></span></asp:HyperLink>
        </ItemTemplate>
        <FooterTemplate>
            </ul></FooterTemplate>
    </asp:Repeater>
</div>
<!--/sidebar-->
