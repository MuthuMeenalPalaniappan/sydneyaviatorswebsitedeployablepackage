﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucGoogleAnalytics.ascx.cs"
    Inherits="SydneyAviators.Web.Controls.ucGoogleAnalytics" %>
<asp:PlaceHolder ID="phSydneyAviators" runat="server" Visible="false">
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-83390-4']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
 
    </script>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phHunterValleyAviation" runat="server" Visible="false">
    <script type="text/javascript">



        var _gaq = _gaq || [];

        _gaq.push(['_setAccount', 'UA-34932481-1']);

        _gaq.push(['_trackPageview']);



        (function () {

            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

        })();

 

    </script>
</asp:PlaceHolder>
