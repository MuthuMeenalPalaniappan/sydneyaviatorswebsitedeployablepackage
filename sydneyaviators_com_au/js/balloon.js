﻿$(document).ready(function () {
    $(".balloonOptions").click(function () {
        var position = $(this).position();
        $(".balloon").css("top", position.top - 23);
        $(".balloon").css("left", position.left + 60);
        $(".balloon").slideToggle();
        return false;
    });

    $('.ballonOptions').blur(function () {
        $('.balloon:visible').hide(500);
    })
});